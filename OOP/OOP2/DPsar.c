/*	--- DVIPUSIS SARASAS ---
 * 		(char)
 * Karolis Jocevičius, PS1
 * 2012
*/
//----------------------------------------------------------------------
int DPerror; 	//Klaidu kintamasis
/* Klaidos:
 * 0 - OK
 * 1 - naujo saraso kintamasis lygus NULL (DPinit)
 * 2 - saraso kintamasis lygus NULL
 * 3 - sarasas tuscias
 * 4 - sarasas->cur == NULL
 * 5 - sarasas->head == NULL
 * 6 - sarasas->tail == NULL
 * 7 - sarasas->tail->nxt == NULL
 * 8 - sarasas->tail->prv == NULL
 * ---------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include "DPsar.h"

//sukuria ir paruosia tuscia sarasa
DPtype *DPinit(){
	DPtype *new = malloc(sizeof(*new));
	DPerror = 0;
	if (new == NULL){
		DPerror=1;
		return NULL;
	}
	new->cnt = 0;
	new->head = NULL;
	new->tail = NULL;
	new->cur = NULL;
	return new;
}


//prideda elementa
DPtype *DPadd(DPtype *s, char x){
	DPelem *new = malloc(sizeof(*new));
	DPerror = 0;
	if (s == NULL) {
		DPerror = 2;
		return NULL;
	}
	
	new->dat = x;
	new->nxt = NULL;
	if (s->cnt == 0) {
		new->prv = NULL;
		s->head = new;
		s->tail = new;
		s->cur = new;
	}
		else{
			new->prv = s->tail;
			s->tail->nxt = new;
			s->tail = new;
		}
	s->cnt++;
	return s;
}

//sunaikina sarasa
DPtype *DPdestr(DPtype *s){
	DPelem *destr = s->tail;
	DPerror = 0;
	if (s == NULL) {
		DPerror = 2;
		return NULL;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	
	do {
		if (s->tail->prv != NULL)
			s->tail = s->tail->prv;
		free(destr);
		destr = s->tail;
		s->cnt--;
	}while (s->cnt != 0);
	free(s);
	return s;
}

/* Nuskaito elementa
	s->cur->dat		*/
char DPread(DPtype *s){
	return s->cur->dat;
}

DPtype *DPreset(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->head == NULL){
		DPerror = 10;
		return s;
	}
	s->cur = s->head;
	return s;
}

DPtype *DPnext(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->cur->nxt == NULL){
		DPerror = 9;
		return s;
	}		
	s->cur = s->cur->nxt;
	return s;
}

DPtype *DPprev(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->cur->prv == NULL){
		DPerror = 9;
		return s;
	}	
	s->cur = s->cur->prv;
	return s;
}
	

//rikiuoja sarasa
DPtype *DPsort(DPtype *s){
	DPelem *chk = NULL;
	char a,b;
	char tmp;
	s->cur = s->head;
	do{
		chk = s->cur->nxt;
		do{
			chk = chk->nxt;
			a = chk->dat;
			b = s->cur->dat;
			if (a < b){
				tmp = s->cur->dat;
				chk->dat = s->cur->dat;
				s->cur->dat = tmp;
			} 
			
		}while (s->cur->nxt != NULL);
		
		s->cur = s->cur->nxt;//gale 
	}while (s->cur != NULL);
	return s;
}
	
	

//error rep
int DPfail(){
	return DPerror;
}

	
