/*	--- DVIPUSIS SARASAS ---
 * Karolis Jocevičius, PS1
 * 2012
*/


//----------------------------------------------------------------------
//---TIPAI--------------------------------------------------------------

//Saraso elemento tipas
//Char masyvų sarasas
typedef struct DPelem {
	struct DPelem *nxt,*prv;		//kitas ir pries einantys el.
	char *dat;						//pointeris i duomenis
}(DPelem);

//Dvipusio saraso tipas
typedef struct DPtype {
	struct DPelem *head,*tail,*cur;	//pirmas ir paskutinis elementai
									// cur - "skaitymo galvute"
	int cnt;						//elementu skaitiklis
}(DPtype);


//----------------------------------------------------------------------
//---FUNKCIJOS----------------------------------------------------------
//error rep
/* Klaidos:
 * 0 - OK :)
 * 1 - malloc fail
 * 2 - NULL
 * 3 - sarasas tuscias
 */
int DPfail();

//init
DPtype *DPinit();

//prideda elementa
DPtype *DPadd(DPtype *s, char *x );

//sunaikina sarasa
DPtype *DPdestr(DPtype *s);

//nuskaito s->cur->dat
char *DPread(DPtype *s);

//grazina s->cur i s->head
DPtype *DPreset(DPtype *s);

//pereina i kita el
DPtype *DPnext(DPtype *s);

//pereina i buvusi el
DPtype *DPprev(DPtype *s);

//isrikiuoja elementus
DPtype *DPsort_str(DPtype *s);
