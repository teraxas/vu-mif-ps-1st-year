/*	--- DVIPUSIS SARASAS ---
 * Karolis Jocevičius, PS1
 * 2012
*/

//----------------------------------------------------------------------
//---TIPAI--------------------------------------------------------------

//Saraso elemento tipas
//Char masyvų sarasas
typedef struct DPelem {
	struct DPelem *nxt,*prv;	//kitas ir pries einantys el.
	char dat;					//pointeris i duomenis
}(DPelem);

//Dvipusio saraso tipas
typedef struct DPtype {
	struct DPelem *head,*tail,*cur;	//pirmas ir paskutinis elementai
									// cur - "skaitymo galvute"
	int cnt;						//elementu skaitiklis
}(DPtype);


//----------------------------------------------------------------------
//---FUNKCIJOS----------------------------------------------------------

/*Suk. tuscia sarasa
 * Pvz:
 * DPtype sar = DPinit(); */
DPtype *DPinit();

//prideda elementą
DPtype *DPadd(DPtype *s, char x);

//sunaikina sąrašą
DPtype *DPdestr(DPtype *s);

/* Nuskaito elementa
	s->cur->dat		*/
char DPread(DPtype *s);

/* Pateikia info apie klaidas
 *     Klaidos:
 * 0 - OK
 * 1 - naujo saraso kintamasis lygus NULL (DPinit)
 * 2 - saraso kintamasis lygus NULL
 * 3 - sarasas tuscias
 * 4 - sarasas->cur == NULL
 * 5 - sarasas->head == NULL
 * 6 - sarasas->tail == NULL
 * 7 - sarasas->tail->nxt == NULL
 * 8 - sarasas->tail->prv == NULL
 * 9 - sarasas->cur->(prv/nxt) == NULL
 * 10 - sarasas->head == NULL
 */
int DPfail();
