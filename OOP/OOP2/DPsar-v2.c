/*	--- DVIPUSIS SARASAS ---
 * 		(char)
 * Karolis Jocevičius, PS1
 * 2012
*/

//----------------------------------------------------------------------
int DPerror=0; 	//Klaidu kintamasis
/* Klaidos:
 * 0 - OK :)
 * 1 - malloc fail
 * 2 - NULL
 * 3 - sarasas tuscias
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "DPsar-v2.h"

//error rep
int DPfail(){
	printf("\n%d\n",DPerror);
	return 0;
}

//sukuria ir paruosia tuscia sarasa
DPtype *DPinit(){
	DPtype *new = malloc(sizeof(*new));
	DPerror = 0;
	if (new == NULL){
		DPerror=1;
		return NULL;
	}
	new->cnt = 0;
	new->head = NULL;
	new->tail = NULL;
	new->cur = NULL;
	return new;
}

//grazina s->cur i s->head
DPtype *DPreset(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->head == NULL){
		DPerror = 2;
		return s;
	}
	s->cur = s->head;
	return s;
}

//prideda elementa
DPtype *DPadd(DPtype *s, char *x ){
	DPelem *new = malloc(sizeof(*new));
	DPerror = 0;
	if (s == NULL) {
		DPerror = 0;
		return s;
	}
	
	new->dat = x;
	new->nxt = NULL;
	if (s->cnt == 0) {
		new->prv = NULL;
		s->head = new;
		s->tail = new;
		s->cur = new;
	}
		else{
			new->prv = s->tail;
			s->tail->nxt = new;
			s->tail = new;
		}
	s->cnt++;
	return s;
}

//sunaikina sarasa
DPtype *DPdestr(DPtype *s){
	DPelem *destr = s->tail;
	DPerror = 0;
	if (s == NULL) {
		DPerror = 2;
		return NULL;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	
	do {
		if (s->tail->prv != NULL)
			s->tail = s->tail->prv;
		free(destr->dat);	
		free(destr);
		destr = s->tail;
		s->cnt--;
	}while (s->cnt != 0);
	free(s);
	return s;
}

//nuskaito s->cur->dat
char *DPread(DPtype *s){
	return s->cur->dat;
}



//pereina i kita el
DPtype *DPnext(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->cur->nxt == NULL){
		DPerror = 2;
		return s;
	}		
	s->cur = s->cur->nxt;
	return s;
}

//pereina i buvusi el
DPtype *DPprev(DPtype *s){
	if (s == NULL){
		DPerror = 2;
		return s;
	}
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	if (s->cur->prv == NULL){
		DPerror = 2;
		return s;
	}	
	s->cur = s->cur->prv;
	return s;
}


//RIKIAVIMAS (string)

//pakeicia raides i mazasias
char *STRcng(char *z){
	int i,l;
	char *rez;
	
	l = strlen(z);
	rez = malloc(l*sizeof(rez));
	
	for (i = 0; i != l; i++){
		rez[i] = tolower(z[i]);
	}
	
	return rez;
}

DPtype *DPsort_str(DPtype *s){
	int i;
	DPelem *e1, *e2;
	DPelem tmp;
	
	//error
	if (s->cnt == 0){
		DPerror = 3;
		return s;
	}
	
	s = DPreset(s);
	
	//sorting
	for (i = 0; i =! s->cnt; i++){
		e1 = s->cur;
		e2 = s->cur->nxt;
		while (e2->nxt != NULL){
			//str palyginimas
			if (strcmp(STRcng(e2->dat),STRcng(e1->dat)) < 0){
				//sukeitimas
				tmp = *e1;
				tmp.nxt = e2->nxt;
				tmp.prv = e2->prv;
				e2->nxt = e1 ->nxt;
				e2->prv = e1 ->prv;
				e1 = e2;
				*e2 = tmp;
			}
			//kitas tikrinamas el.
			if (e2->nxt != NULL)
				e2 = e2->nxt;
		}
		//kitas saraso el.
		s = DPnext(s);
		
	}
return s;	
}
