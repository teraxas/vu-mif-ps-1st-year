/*	Karolis Jocevičius, PS1
 *  2012
*/

/* 24. Nuskaityti vieną failą. Sukurti dvipusį sąrašą, kurio elementai eilutės. 
 * Į jį patalpinti žodžius, kurie baigiasi nurodyta galūne (ne ilgesne nei 3 simboliai). 
 * Jį surūšiuoti pagal abėcėlę. Rezultatą išvesti į atskirą failą.

	Reikalavimai:
	1. Sukurti multifailinį projektą. Dinaminės duomenų struktūros(-ų) apibrėžimą ir jos operacijas
realizuoti atskirame faile (modulyje(-iuose)).
	2. Pradinius duomenis nuskaityti iš vieno ar kelių tekstinių failų (kaip nurodyta sąlygoje).
	3. Išanalizuoti failą kaip nurodyta užduotyje, informaciją patalpinti į dinamines duomenų
struktūras. Jei nurodyta užduotyje sukurti kelis struktūrų egzempliorius. Pastaba: kad atskirti
žodžius tekste (jei taikytina), galima apsibrėžti skirtukų aibę, pvz. atskirame faile.
	4. Jei reikia, atlikti papildomas (pvz. rūšiavimo, statistikos paskaičiavimo ir pan.) operacijas.
	5. Rezultatinės struktūros(-ų) turinį išvesti į failą(-us).
	6. Pageidautina, kad įvedimo ir išvedimo failų vardai būtų perduodami programai kaip
komandinės eilutės parametrai (tiek kiek jų reikia programai).
	7. Programa turi būti apsaugota nuo nekorektiškų parametrų ar nuskaitomų duomenų – turi išvesti
informatyvius klaidų pranešimus. */

/* --- PLAN ---
 * readCHK <- file pointer, galune
 * sort
 * write <- out file pointer, DP sar
 */
//	gcc -o antr main.c DPsar-v2.c endCHK.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "endCHK.h"
#include "DPsar.h"




DPtype *doit(FILE *f, char chk[3]){
	DPtype *s;
	char c;
	char *w = malloc(20*sizeof(*w));
	s = DPinit();
	
	printf("\n'%s' is chk",chk);	//PROBLEM?
//	printf("\nDPerror %d",DPerror());
	DPerror();
	do {
		c=getc(f);
		if (IFspc(c) == 1){
			if (cnt == 2){
				
				printf("\n%s",w);
				s=DPadd(s,*w);
				printf("\n%d, doit\n",s->cnt);
			//	printf("\nDPerror %d",,DPerror());
			}//T
			w[0] = '\0';
		    cnt = 0;
		};
			
		if (IFspc(c) == 0){
			if (IFa(c, chk[cnt]) == 1){
				strncat(w,&c,1);
				cnt++;
			//	printf("\n%s",w);
			}
				else{
					strncat(w,&c,1);
					cnt = 0;
				//	printf("\n%s",w);
				}
		};
	} while (c != EOF);
	return s;
}

int write(DPtype *s/*, FILE f*/){
	s = DPreset(s);
	printf("\n\n");
	do {
		printf("%s\n",DPread(s));
		s = DPnext(s);
	} while (s->cur != NULL);
	//fprintf
	return 0;
}


int main ( int argc, char *argv[] )
{
	DPtype *sar;
	
//	printf("\nDPerror %d",DPerror());//T
	
	if ( argc != 4 ) /* argc should be 4 for correct execution */
	{
		printf( "naudojimas: %s input output check[3]\n", argv[0] );
		return -1;
	} 
		
		
	char chk[3];
		
	
	// We assume argv[1] is a filename to open
	FILE *inp = fopen( argv[1], "r" );
	FILE *out = fopen( argv[2], "w" );
	chk[0] = argv[3][0];
	chk[1] = argv[3][1];
	chk[2] = argv[3][2]; 
	
	printf("\n'%s' is chk",chk);	//PROBLEM?
	
	/* fopen returns 0, the NULL pointer, on failure */
	if ( inp == 0 || out == 0){
			printf( "Could not open file\n" );
			return -1;
		}
	
	
		
//	sar = DPinit();
	
	sar = doit(inp,chk);
	
//	write(sar);
	printf("\n%d\n",sar->cnt);//T
	sar = DPsort_str(sar);
//	printf("\nDPerror %d",,DPerror());//T
	printf("\n%d\n",sar->cnt);//T
//	write(sar);
	
			fclose( inp );
			fclose( out );
		
	
	return 0;
}

