/* 24. Nuskaityti vieną failą. Sukurti dvipusį sąrašą, kurio elementai eilutės. 
 * Į jį patalpinti žodžius, kurie baigiasi nurodyta galūne (ne ilgesne nei 3 simboliai). 
 * Jį surūšiuoti pagal abėcėlę. Rezultatą išvesti į atskirą failą.

	Reikalavimai:
	1. Sukurti multifailinį projektą. Dinaminės duomenų struktūros(-ų) apibrėžimą ir jos operacijas
realizuoti atskirame faile (modulyje(-iuose)).
	2. Pradinius duomenis nuskaityti iš vieno ar kelių tekstinių failų (kaip nurodyta sąlygoje).
	3. Išanalizuoti failą kaip nurodyta užduotyje, informaciją patalpinti į dinamines duomenų
struktūras. Jei nurodyta užduotyje sukurti kelis struktūrų egzempliorius. Pastaba: kad atskirti
žodžius tekste (jei taikytina), galima apsibrėžti skirtukų aibę, pvz. atskirame faile.
	4. Jei reikia, atlikti papildomas (pvz. rūšiavimo, statistikos paskaičiavimo ir pan.) operacijas.
	5. Rezultatinės struktūros(-ų) turinį išvesti į failą(-us).
	6. Pageidautina, kad įvedimo ir išvedimo failų vardai būtų perduodami programai kaip
komandinės eilutės parametrai (tiek kiek jų reikia programai).
	7. Programa turi būti apsaugota nuo nekorektiškų parametrų ar nuskaitomų duomenų – turi išvesti
informatyvius klaidų pranešimus. */
#define wrd = 50;
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "llist.h"
#include "checks.h"

llist *perrink(llist *s,char *chk){
	char *zod;
	
	llReset(s);
	do{
		zod = llGetVal(s);
		if (IFa(zod,chk) == 0)
			llRemCur(s);
		llGoNxt(s);
	} while (llFail() == 0);
	llReset(s);
	return s;
}

llist *rink(llist *s, FILE *f){
	char *zod = malloc(50*sizeof(*zod));
	char c;
	int cnt = 0;
	
	do{
		if (IFspc(c) == 1){
			cnt++;
			if (cnt == 1){
				llAdd(s,zod);
				zod = malloc(50*sizeof(*zod));
			}
		}
		else{
			cnt = 0;
			strncat(zod,&c,1);
		}
	c = fgetc(f);
	} while (c != EOF);
	llAdd(s,zod);
	return s;
}

int print(llist *s){
	char *zod;
	
	llReset(s);
	
	do{
		zod = llGetVal(s);
		printf("\n%s",zod);
		llGoNxt(s);
	} while (llFail() == 0);
	printf("\n");
	return 0;
}

int fprint(llist *s,FILE *f){
	char *zod;
	
	llReset(s);
	
	do{
		zod = llGetVal(s);
		fprintf(f,"%s\n",zod);
		llGoNxt(s);
	} while (llFail() == 0);
	
	return 0;
}

int main(int argc, char **argv)
{
	char chk[4];
	llist *sar;
	
	
//----------------------------------------------------------------------
	//tikrinami parametrai
	if ( argc != 4 ) // parametrai turi buti 4 (programa input output galune)
	{
		printf("\n\nKlaidingai ivesti parametrai!!!\n");
		printf( "naudojimas: %s input output check[3 simboliai]\n\n", argv[0] );
		return -1;
	}
	//Atidarom failus
	FILE *inp = fopen( argv[1], "r" );
	FILE *out = fopen( argv[2], "w" );
	//Paimam 3 tikrinamus simbolius
	chk[0] = argv[3][0];
	chk[1] = argv[3][1];
	chk[2] = argv[3][2];
	printf("\nTikrinama galune: %s\n",chk);	//T
//----------------------------------------------------------------------
	sar = llInit();
	
	rink(sar,inp);
	print(sar);
	printf("\n");
	
	perrink(sar,chk);
//	print(sar);
	
	llSortStr(sar);
	print(sar);
	
	fprint(sar,out);
	
	llDestr(sar);
	
	
//----------------------------------------------------------------------
	fclose(inp);
	fclose(out);
	
//	printf("\nOK :)\n");//T
	return 0;
}

