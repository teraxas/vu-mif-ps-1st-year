#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "checks.h"



/*Ar tarpas/skirtukas?
 * 0 - nope :(
 * 1 - tarpas :)	*/
int IFspc(char s){
	if (s == ' ' || s == EOF || s == '\n' || s == ',' || s == '.')
		return 1;
	return 0;
}

int IFa(char *s, char *chk){
	int l = strlen(s)-1;
	int l2 = strlen(chk)-1;
	int x = l - l2;	
	
	if (x<0)
		return 0;
	
	if (l2 == 1 && s[l] == chk[l2])
		return 1;
	
	if (l2 == 2 && s[l] == chk[l2] && s[l-1] == chk[l2-1])
		return 1;

	if (l2 == 3 && s[l] == chk[l2] && s[l-1] == chk[l2-1] && s[l-2] == chk[l2-2])
		return 1;


	return 0;
}
