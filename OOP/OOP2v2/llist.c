/*	Karolis Jocevičius, PS1
 *  2012
*/

//Errors
int llF=0;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "llist.h"


int llFail(){
	return llF;
}

//----------------------------------------------------------------------
llist *llInit(){
	llist *s = malloc(sizeof(*s));
	llF = 0;
	if (s == NULL){
		llF = 1;
		return NULL;
	}
	
	s->cnt = 0;		//problem?
	s->cur = NULL;
	s->head = NULL;
	s->tail = NULL;
	
	return s;
}


llist *llAdd(llist *s,void *x){
	llE *new = malloc(sizeof(*new));
	
	llF = 0;
	
	if (new == NULL){
		llF = 1;
		return NULL;
	}
	
	new->dat = x;
	new->nxt = NULL;
	
	
	//jei tuscias
	if (s->cnt == 0){
		new->prv = NULL;
		s->tail = new;
		s->head = new;
		s->cur = new;
		s->cnt++;
		return s;
	}
	s->tail->nxt = new;
	new->prv = s->tail;
	s->tail = new;
	s->cnt++;
	return s;	
}

void *llGetVal(llist *s){
	void *ret;
	
	llF = 0;
	if (s == NULL){
		llF = 1;
		return s;
	}
	if (s->cur == NULL){
		llF = 2;
		return s;
	}

	ret = s->cur->dat;
	return ret;
}

llist *llGoNxt(llist *s){
	llF = 0;
	if (s == NULL){
		llF = 1;
		return s;
	}
	if (s->cur == NULL){
		llF = 2;
		return s;
	}
	if (s->cur->nxt == NULL){
		llF = 3;
		return s;
	}
	
	s->cur = s->cur->nxt;
	return s;
}

llist *llGoPrv(llist *s){
	llF = 0;
	if (s == NULL){
		llF = 1;
		return s;
	}
	if (s->cur == NULL){
		llF = 2;
		return s;
	}
	if (s->cur->prv == NULL){
		llF = 3;
		return s;
	}
	
	s->cur = s->cur->prv;
	return s;
}

llist *llRemCur (llist *s){
	llF = 0;
	if (s == NULL){
		llF = 1;
		return s;
	}
	if (s->cur == NULL){
		llF = 2;
		return s;
	}
	
//	free(s->cur->dat);
	
	if (s->cur->prv == NULL)
		s->head = s->cur->nxt;
	if (s->cur->nxt == NULL)
		s->tail = s->cur->prv;
	if (s->cur->nxt != NULL)
		s->cur->nxt->prv = s->cur->prv;
	if (s->cur->prv != NULL)
		s->cur->prv->nxt = s->cur->nxt;
		
	free(s->cur);
	s->cur = s->head;
	s->cnt--;
	return s;
}

llist *llReset (llist *s){
	s->cur = s->head;
	return s;
}

llist *llDestr (llist *s){
		llF = 0;
	if (s == NULL){
		llF = 1;
		return s;
	}
	if (s->cur == NULL){
		llF = 2;
		return s;
	}
	
	llReset(s);
	while (s->cur != NULL){
		llRemCur(s);
		llGoNxt(s);
	}
	
	s = NULL;
	
	return s;
}


//----------------------------------------------------------------------
// String Sort

//keicia visas raides i mazasias
char *STRcng(char *z){
	int i,l;
	char *rez;
	
	l = strlen(z);
	rez = malloc(l*sizeof(rez));
	
	for (i = 0; i != l; i++){
		rez[i] = tolower(z[i]);
	}
	
	return rez;
}


llist *llSortStr(llist *s){
	int i;
	char *e1, *e2;
	llist s1,s2;
	char *tmp;
	
	//error
	if (s->cnt == 0){
		llF = 4;
		return s;
	}
	
	llReset(s);
	
	s1 = *s;
	//sorting
	for (i = 0; i != s->cnt; i++){
		s2 = s1;
		e1 = llGetVal(&s1);
		llGoNxt(&s2);
		do{
			//Get values
			e2 = llGetVal(&s2);
			//str palyginimas
			if (strcmp(STRcng(e1),STRcng(e2)) > 0){
				//sukeitimas
			//	printf("\nkeisim %s ir %s",e1,e2);//Test
				tmp = malloc(strlen(s1.cur->dat)*sizeof(*tmp));
				strcpy(tmp,s1.cur->dat);
				strcpy(s1.cur->dat,s2.cur->dat);
				strcpy(s2.cur->dat,tmp);
			}
			//kitas tikrinamas el.
			llGoNxt(&s2);
			
		}while (llFail() == 0);
		//kitas saraso el.
		llGoNxt(&s1);
		
	}
	s=&s1;
return s;	
}
