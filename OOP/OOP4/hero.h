#ifndef HERO_H
#define HERO_H

using namespace std;

class hero{
	public:
		hero();
		
		//health,mana,armor,defense points
		int hp,mp,ap,Pdp,Mdp;
		
		//strength, intellingence
		int str,intel;

		//char level
		int lvl;

		//modifiers (physical, magical)
		int pMOD,mMOD;
		
		char name[20];
		
		int mpMAX,hpMAX;
		
		void hName(char n[20]);
		
		void hGetHit(int Phit,int Mhit);
		int hMakePhit();
		int hMakeMhit();
		
		void hHeal();
		void hArmor(int arm);
		void hSetlvl(int l);

		void hPrintInfo();
		
		~hero();
	private:
		
		
};

#endif
