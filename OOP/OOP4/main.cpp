#include <iostream>
#include <cstdlib>
#include <istream>

#include "hero.h"
#include "warior.h"
#include "mage.h"


void PrintLegend(){
	cout << "\n1 - Smugis\n"\
			"2 - Burtas\n"\
			"3 - Pasigydimas\n"\
			"4 - Specialus (magui - vogti hp, museikai - piktas museika)"\
			"q - Uzbaigti\n";
}

int main(int argc, char **argv)
{
	char name[20], name1[20];
	int lvl;
	warior sir;
	cout << "\nLabas, ivesk museikos varda[20] : ";
	cin >> name;
	cout << "\n";
	cout << "Ir lygi: ";
	cin >> lvl;
	
	sir.hName(name);
	sir.hSetlvl(lvl);
	sir.hPrintInfo();

	mage wiz;
	cout << "\nDabar vesk mago varda[20] : ";
	cin >> name1;
	cout << "\n";	
	cout << "Ir lygi: ";
	cin >> lvl;
	
	wiz.hSetlvl(lvl);
	wiz.hName(name1);
	wiz.hPrintInfo();
	
	char x;
	
	cout << "\n Kova prasidejo!!!, pradeda magas\n";
	do{
		if (wiz.hp > 0){
		PrintLegend();
		cout << "\n Ka darys "<<wiz.name<<"? ";
		cin >> x;
		switch(x){
				case '1':
					sir.hGetHit(wiz.hMakePhit(),0);
					break;
				case '2':
					sir.hGetHit(0,wiz.hMakeMhit());
					break;
				case '3':
					sir.hHeal();
					break;
				case '4':
					sir.hGetHit(0,wiz.MagStealHP());
					break;
				case 'q':
					break;
				default:
					cout << "\nPraleido ejima";
					break;
			}
			wiz.hPrintInfo();
			sir.hPrintInfo();
		}
		
		if (sir.hp > 0 && wiz.hp > 0){
			PrintLegend();
			cout << "\n Ka darys "<<sir.name<<"? ";
			cin >> x;
			switch(x){
				case '1':
					wiz.hGetHit(sir.hMakePhit(),0);
					break;
				case '2':
					wiz.hGetHit(0,sir.hMakeMhit());
					break;
				case '3':
					sir.hHeal();
					break;
				case '4':
					sir.warBerserk();
				case 'q':
					break;
				default:
					cout << "\nPraleido ejima";
					break;
			}
		}
		wiz.hPrintInfo();
		sir.hPrintInfo();
	}while (sir.hp !=0 && wiz.hp !=0 && x != 'q');
		if (sir.hp == 0) {
			cout << "\n" << sir.name << " zuvo dvikovoje\n";
		}
		if (wiz.hp == 0) {
			cout << "\n" << wiz.name << " zuvo dvikovoje\n";
		}
	return 0;
}

