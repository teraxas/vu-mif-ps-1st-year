/*
 * mage.cpp
 * 
 * Copyright 2012 Karolis Jocevičius <karolis.jocevicius@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "mage.h"


mage::mage() : hero()
{
	pMOD = 1;
	mMOD = 2;
	mpMAX = 100;
	mp = mpMAX;
	hpMAX = 50;
	hp = hpMAX;
	str = 100;
	intel = 50;
	Pdp = 5;
	Mdp = 1;
}

int mage :: MagStealHP(){
	if (mp > 30){
		hp = hp + (intel / 20);
		if (hp > hpMAX) hp = hpMAX;
		mp = mp - 30;
		return (intel/15);
	}
	return 0;
}


