#include <cstdlib>
#include <iostream>
#include <string.h>
#include "hero.h"

//constructor
hero :: hero(){
	hpMAX = 100;
	mpMAX = 100;
	hp = hpMAX;
	mp = mpMAX;
	ap = 0;
	Pdp = 0;
	Mdp = 0;
	str = 100;
	intel = 100;
	lvl = 1;
	pMOD = 10; mMOD = 10;
}

void hero :: hName(char n[20]){
	strncpy(name,n,20);
}

void hero :: hSetlvl(int l){
	int l2 = l-lvl;
	
	lvl = l;
	
	hpMAX = hpMAX + pMOD * l2;
	hp = hpMAX;
	mpMAX = mpMAX + mMOD * l2;
	mp = mpMAX;
	
	
	str = str + pMOD * l2;
	intel = intel + mMOD * l2;
	
	Pdp = Pdp + pMOD * l2;
	Mdp = Mdp + mMOD * l2;
}

void hero :: hGetHit(int Phit, int Mhit){
	Phit = Phit - (Pdp/15) - ap;
	Mhit = Mhit - (Mdp/15);
	
	if (Phit < 0) Phit = 0;
	if (Mhit < 0) Mhit = 0;
	
	hp = hp - Mhit - Phit;
	ap = ap - Phit;
	if (hp < 0) hp = 0;
	if (ap < 0) ap = 0;
}


int hero :: hMakePhit(){
	int hit = 0;
	hit = str/15;
	return hit;
}
int hero :: hMakeMhit(){
	int hit = 0;
	hit = intel/10;
	if (mp > hit){
		mp = mp - hit;
		if (mp < 0) mp = 0; 
		return hit;
	}
	return 0;
}


void hero :: hHeal(){
	int hl;
	hl = intel/(10/mMOD);
	hp = hp + hl;
	mp = mp - hl;
	if (hp > hpMAX) hp = hpMAX;
}

void hero :: hArmor(int arm){
	ap = ap + arm;
	if (ap > 100) ap = 100;
}

void hero :: hPrintInfo(){
	cout << "lvl "<<lvl<<" "<<name<<"\n"\
		<<"hp "<<hp<<"\n"\
		<<"mp "<<mp<<"\n"\
		<<"ap "<<ap<<"\n"\
		<<"Pdp "<<Pdp<<", Mdp "<<Mdp<<"\n";
}

//destructor
hero :: ~hero(){
   

}
	 
