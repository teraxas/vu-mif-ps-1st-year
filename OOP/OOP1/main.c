/* 24. Nuskaityti vieną failą. Sukurti dvipusį sąrašą, kurio elementai eilutės. 
 * Į jį patalpinti žodžius, kurie baigiasi nurodyta galūne (ne ilgesne nei 3 simboliai). 
 * Jį surūšiuoti pagal abėcėlę. Rezultatą išvesti į atskirą failą.

	Reikalavimai:
	1. Sukurti multifailinį projektą. Dinaminės duomenų struktūros(-ų) apibrėžimą ir jos operacijas
realizuoti atskirame faile (modulyje(-iuose)).
	2. Pradinius duomenis nuskaityti iš vieno ar kelių tekstinių failų (kaip nurodyta sąlygoje).
	3. Išanalizuoti failą kaip nurodyta užduotyje, informaciją patalpinti į dinamines duomenų
struktūras. Jei nurodyta užduotyje sukurti kelis struktūrų egzempliorius. Pastaba: kad atskirti
žodžius tekste (jei taikytina), galima apsibrėžti skirtukų aibę, pvz. atskirame faile.
	4. Jei reikia, atlikti papildomas (pvz. rūšiavimo, statistikos paskaičiavimo ir pan.) operacijas.
	5. Rezultatinės struktūros(-ų) turinį išvesti į failą(-us).
	6. Pageidautina, kad įvedimo ir išvedimo failų vardai būtų perduodami programai kaip
komandinės eilutės parametrai (tiek kiek jų reikia programai).
	7. Programa turi būti apsaugota nuo nekorektiškų parametrų ar nuskaitomų duomenų – turi išvesti
informatyvius klaidų pranešimus. */

#include <stdio.h>
#include <stdlib.h>
#include "DPsar.h"
int main()
{
	
	return 0;
}

