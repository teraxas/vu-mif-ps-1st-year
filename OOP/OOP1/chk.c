#include <stdio.h>
#include <stdlib.h>

#include "chk.h"

int chk1(int sk)	//jei sk prasideda skaitmeniu 1, tai chk1=1
{					//jei sk NEprasideda skaitmeniu 1, tai chk1=0
	int k;
	k=0;
		if ((sk==1) || (sk==-1)) k=1;
			else if ((sk<10) && (sk>-10)) k=0;
					else do { 
							sk = sk / 10;
							if ((sk==1) || (sk==-1))  k=1;
						} while ((sk>10) || (sk<-10)); 
	return k;				
}
