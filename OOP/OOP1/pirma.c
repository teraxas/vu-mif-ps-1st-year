// 20. Įvesti sveikų skaičių (gali būti tiek neigiami tiek teigiami) seką, 
//	kurios pabaiga žymima skaičiumi 0.
//	Išvesti sekos narių, kurie prasideda skaitmeniu 1, skaičių.
// REIKALAVIMAI:
//		Panaudoti, įvedimo ir išvedimo funkcijas.
// 		Apibrėžti bent vieną funkciją su parametrais,
//		sukurti antraštės failą. 
//		Pagrindinius programos kintamuosius sukurti dinaminėje atmintyje
//		panaudoti dinaminės atminties paskirstymo funkcijas.
// iki 28 d?
//gcc -o pirma pirma.o chk.o
#include <stdio.h>
#include <stdlib.h>

#include "chk.h"
#include "chk2.h"

int main()
{
	int *sk = malloc( sizeof(int) );
	int counter=0;
	printf("Vesk seka!\n");
	do {
		scanf("%d",sk);
		counter=counter+chk1(*sk);
		printf(" counter: %d",counter);	//check
		printf("\n");
		} while (*sk != 0);
	free(sk);
	printf("\n\nAts: %d",counter);
	printf("\n\n");
	return 0;
}
