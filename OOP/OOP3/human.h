/*
 * human.h
 * 
 * Copyright 2012 Karolis Jocevičius <karolis.jocevicius@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef HUMAN_H
#define HUMAN_H

#include "hero.h"

class human: public hero
{
	public:
		human();
		virtual ~human();
	
	private:
		/* add your private declarations */
};

#endif /* HUMAN_H */ 
