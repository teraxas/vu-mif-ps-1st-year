#include <cstdlib>
#include <iostream>
#include <string.h>
#include "hero.h"

//constructor
hero :: hero(){
	hpMAX = 100;
	mpMAX = 100;
	hp = hpMAX;
	mp = mpMAX;
	ap = 0;
	dp = 0;
	lvl = 1;
}

void hero :: hName(char n[20]){
	strncpy(name,n,20);
}

void hero :: hSetlvl(int l){
	lvl = l;
}

void hero :: hAttack(int hit){
	hp = hp - (hit - ap - dp);
	ap = ap - hit;
	if (hp < 0) hp = 0;
	if (ap < 0) ap = 0;
}

void hero :: hHeal(int hl){
	hp = hp + hl;
	if (hp > 100) hp = 100;
}

void hero :: hArmor(int arm){
	ap = ap + arm;
	if (ap > 100) ap = 100;
}

void hero :: hDefense(int d){
	dp = d;
}

//destructor
hero :: ~hero(){
}
	 
