//karolis Jocevicius PS1

#include <iostream>
#include <istream>
#include <cstdlib>
#include <stdio.h>
#include "hero.h"

using namespace std;

void help(){
	cout << "Veiksmai:\n"\
			"1 - uzvosk herojui\n"\
			"2 - pagydyk heroju\n"\
			"3 - uzmauk sarvus\n"\
			"c - herojaus informacija\n"\
			"0 - gana, baigiam\n"\
			"Tai ka darom?...\n";
}

int main(int argc, char **argv)
{
	hero chuck;
	
	char name[20];
	cout << "\nLabas, ivesk herojaus varda[20] : ";
	cin.getline (name, 20, '\n');
	cout << "\n";
	chuck.hName(name);
	
	char k;
	int h;
	
	do{
		help();
		cin >> k;
		switch(k){
			case '0':
				break;
			case '1':
				cout << "\n Kaip stipriai uzvosim? ";
				cin >> h;
				chuck.hAttack(h);
				cout <<"\n";
				break;
			case '2':
				cout << "\n Kiek pagydysim? ";
				cin >> h;
				chuck.hHeal(h);
				break;
			case '3':
				cout << "\nKokio kietumo sarvus maunam? ";
				cin >> h;
				chuck.hArmor(h);
				break;
			case '4':
				cout << "\nkiek kietai ponas "<<chuck.name;
				cout << "ginsis? ";
				cin >> h;
				chuck.hDefense(h);
			case 'c':
				cout << "\n Tavo herojus ponas "<<chuck.name\
						<<"\n HP "<<chuck.hp\
						<<"/"<<chuck.hpMAX\
						<<"\n MP "<<chuck.mp\
						<<"/"<<chuck.mpMAX\
						<<"\n AP "<<chuck.ap\
						<<"\n DP "<<chuck.dp\
						<<"\n";
				break;
			}
	}while (k != '0');
	
	
	
	return 0;
}

