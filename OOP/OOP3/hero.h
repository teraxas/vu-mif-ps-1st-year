#ifndef HERO_H
#define HERO_H

using namespace std;

class hero{
	public:
		hero();
		
		//health,mana,armor,defense points,gold,xp
		int hp,mp,ap,dp;

		int lvl;
		
		char name[20];
		
		int mpMAX,hpMAX;
		
		void hName(char n[20]);
		void hAttack(int hit);
		void hHeal(int hl);
		void hArmor(int arm);
		void hSetlvl(int l);
		void hDefense(int d);
		~hero();
	private:
		
		
};

#endif
