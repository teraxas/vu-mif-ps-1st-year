;Karolis Jocevičius, PS1
;--------------------------------------------------------------------------------
;13.	*	Programa, sudedanti du beveik bet kokio ilgio dešimtainius skaičius. 
;		*	Pradiniai skaičiai ir rezultatas failuose. 
;			Pvz.: fadd10 file1.txt file2.txt result.txt		//pataisyk exe vardą
;		*	Parametrai
;--------------------------------------------------------------------------------

.model small
.stack 100h

.data 
	Help			db 'Programa sudeda du beveik bet kokio ilgio skaicius',10,13
					db 'Parametrai paduodami tokia tvarka: sumuok.exe duom1.txt duom2.txt rez.txt',10,13
					db 10,13,'Autorius: Karolis Jocevicius',10,13,'$'
	Klaidele1		db 'KLAIDA  !!!!!!!!!!!!  Iveskite /? pagalbai                  !!!!!!!!!!!!!$'
	Klaidele2		db 'KLAIDA  !!!!!!!!!!!!  Rasti simboliai, kurie nera skaiciai  !!!!!!!!!!!!!$'
	
	f1		db 20 dup (?)							
	f1h		dw ?									
	f2		db 20 dup (?)					
	f2h		dw ?
	f3		db 20 dup (?)
	f3h		dw ?
;---------------------------------------|
;filename	|	handle	|	size		|
;-----------------------|---------------|
;f1			|	f1h		|	file1Size	|
;f2			|	f2h		|	file2Size	|
;f3			|	f3h		|				|
;---------------------------------------|

	pos1			dw 0, 0							;kus esam skaiciuojant baitais nuo pradzios
	pos2			dw 0, 0
	pos3			dw 0, 0	
									 
	file1Size 	dw 0, 0								;1 failo dydis 
	file2Size 	dw 0, 0
	
	fbuf1		 	db 10 dup (?)					
	fbuf2		 	db 10 dup (?)
	fbuf3		 	db 10 dup (?)
		
	fbuf1size 	dw 0
	fbuf2size 	dw 0
	fbuf3size 	dw 0
	
	tmpRegs	 		dw 4 dup (?)			;registru buferis, naudojamas tik writebuf proc
	newL			db 10, 13, '$'
.code
 
	mov ax, @data
	mov ds, ax
	mov bx, 0081h 
Ieskok:
	mov ax, es:[bx]							; i ax irasom du baitus, i kuriuos rodo registro turinio adresas bx
	cmp al,0Dh								; tikrinam, ar tai ne pabaigos simbolis 
	je	Nera								; jei taip, vadinasi neradau '/?'
	cmp ax,3F2Fh							; o gal nuskaityta '/?' 
	je	Yra										 
	inc bx												
	jmp Ieskok										

Yra:														
	mov ah, 09h										 
	mov dx, offset Help					 
	int 21h
	jmp Pabaiga									 

;-----------------------------------------------------------------------
;1 duom failo vardas
;-----------------------------------------------------------------------

Nera:
	mov si, 0080h								 
	
SpcSrc1:								;ieskom komandineje eiluteje simbolio nelygaus tarpui
	inc si												
	mov al, es:[si]							;i al irasom viena simboli, nuskaityta is komandines eilutes
	cmp al, ' '										 
	je	SpcSrc1				
	
	mov bx, offset f1					
NmSrc1: 
	mov al, es:[si]							;i al irasom viena simboli, nuskaityta is komandines eilutes
	
	cmp al, 0Dh								;patikrinam ar nuskaitytas simbolis nera Enter	
	jne IeskokToliau1						 
	jmp Klaida									
	
IeskokToliau1:
	cmp al, ' '									 
	je	SpcSrc2							;jei tai yra Enter persokam i 'SpcSrc2'
	mov ds:[bx],al						;i pirmojo failo varda irasom simboli is komandines eilutes
	inc si									 
	inc bx
	jmp NmSrc1							;persokam i 'NmSrc1', pildom iki galo 1 duomenu failo varda

;-----------------------------------------------------------------------
;2 duom failo vardas
;-----------------------------------------------------------------------

SpcSrc2:									 ;ieskom komandineje eiluterja ar nera tarpu
	inc si												
	mov al, es:[si]				 
	cmp al, ' '									 
	je	SpcSrc2							
	mov bx, offset f2					

NmSrc2:
	mov al, es:[si]							 ;i al irasom viena simboli, nuskaityta is komandines eilutes
	cmp al, 0Dh									 
	jne IeskokToliau2						 
	jmp Klaida									

IeskokToliau2:
	cmp al, ' '									 
	je	SpcSrc3							
	mov ds:[bx], al							 ;i pirmojo failo varda irasom simboli is komandines eilutes
	inc si	
	inc bx
	jmp NmSrc2 

;-----------------------------------------------------------------------
;Rezultato failo vardo ieskojimas ir irasymas
;-----------------------------------------------------------------------

SpcSrc3:									 ;ieskom ar komandineje eiluteje nera tarpu
	inc si												
	mov al, es:[si]							 
	cmp al, ' '									 
	je	SpcSrc3							
	mov al, es:[si]							 ;i al irasom ta simboli nelygu tarpui
	cmp al, 0Dh									 ; patikrinam ar irasytas simbolis nera Enter
	jne IeskokToliau3						 
	jmp Klaida									 

IeskokToliau3: 
	mov bx, offset f3					 
NmSrc3:
	mov al, es:[si]							 
	inc si												
	cmp al, 0Dh									 
	je Dirbam

	cmp al, ' '									 
	je NmSrc3								;jei tai yra tarpas, tai tikrinsiu tolimesni komandines eilutes simboli, persokam i'NmSrc3'
	mov ds:[bx], al							;i pirmojo failo varda irasom simboli is komandines eilutes
	inc bx												
	jmp NmSrc3							 



	
Dirbam:	

	mov ax, ds								;ds sulyginam su es, nes naudosim di registra adresavimui
	mov es, ax											
;-----------------------------------------------------------------------
;atidarom failus	
;-----------------------------------------------------------------------
	mov ax, offset f1
	call atidarytuvas
	mov [f1h], ax
	
	mov ax, offset f2
	call atidarytuvas
	mov [f2h], ax
;-----------------------------------------------------------------------
;randam failu dydzius
;-----------------------------------------------------------------------
	mov ax, [f1h]
	call raskDydi
	mov [file1Size], ax 
	mov [file1Size + 2], dx
	mov [pos1], ax
	mov [pos1 + 2], dx
	
	mov ax, [f2h]
	call raskDydi
	mov [file2Size], ax
	mov [file2Size + 2], dx
	mov [pos2], ax
	mov [pos2 + 2], dx
	
	
	call gaminam							;sukuriam rezultatu faila
	call raskilgesni						;randa ilgesnio failo ilgi
	
	call suma
;-----------------------------------------------------------------------
;UZDAROM FAILUS
;-----------------------------------------------------------------------
	mov ax, [f1h]									
	call uzdarytuvas
	
	mov ax, [f2h]
	call uzdarytuvas
	
	mov ax, [f3h]
	call uzdarytuvas	
;-----------------------------------------------------------------------
;BAIGIAM
;-----------------------------------------------------------------------
Pabaiga:
	mov ah, 4Ch
	int 21h 
;-----------------------------------------------------------------------
;PROCEDŪROS
;-----------------------------------------------------------------------
Klaida2:
	mov ah, 4Ch
	int 21h
	
Klaida3:
	mov dx, offset Klaidele2
	call spausdink
	mov ah, 4Ch
	int 21h	
	
suma proc
	call atnBuf1							 ;jamam 10 baitu ar maziau jei tiek nera (nuo galo)
	cmp	[fbuf1size], 0
	jle	Klaida2
	call atnBuf2
	cmp [fbuf2size], 0
	jle	Klaida2
	mov	di, offset fbuf1				 ;padarom kad di rodytu i pirmo buferio gala, o si i antro
	add	di, [fbuf1Size]
	mov	si, offset fbuf2
	add	si, [fbuf2Size]
	xor	bx, bx									 ;nulinam nes yra liekana ir pernesimas
	xor	cx, cx									 ;valom nes counteris, kiek skaitmenu bus buferi 3
	xor	dx, dx
Loopas:
	xor ax, ax
	mov al, [di - 1]							;di-1 rodo i skaitmeni kuris dar neapdirbtas
	sub al, '0'
	mov ah, [si - 1]
	sub ah, '0'

	cmp ah, 9										 ;ar normalus skaiciai
	ja	Klaida3
	cmp al, 9
	ja	Klaida3
		
	
	add al, ah										;sudedam skaitmenis
	add al, bl										;pridedam skola 
	mov bl, 10
	xor ah, ah										;nulinam nes jau sudejom bet ah liko skaitmuo
	div bl											
	mov bx, ax										; bl turim liekana kuria reiks pernest(kas minty), bh turim susumuota skaitmeni

	xor ah, ah										;sukisam paskaiciuota skaitmeni i steka
	mov al, bh
	push ax 
	inc cx												;skaiciuojam kiek steke skaitmenu
	
	dec di												;sumazinam di ir si kad rodytu i kitus skaitmenis
	dec si
	cmp di, offset fbuf1					 ;tikrinam di(buferio paskutinio neapdoroto baito adresas) su buferio 0 baitu jei jis bus mazesnis reiskia buferis pasibaige	
	jle	bufer1isdead
	cmp si, offset fbuf2
	jle	bufer2isdead
	jmp Loopas

bufer1isdead:										;jei buferiai pasibaige 
	call formbuf									;suformuojam rez buferi
	call writebuf									;cia ji irasom
	call atnBuf1									;nusiskaitom nauja buferi 
	cmp [fbuf1size], 0						 		;patikrinam ar nesibaige failas
	jle print2										 
	mov	di, offset fbuf1							;jei nesibaige sureguliuoja kad vel i gala rodytu
	add	di, [fbuf1Size] 

	jmp checkbuff2								 	;buferis jau suformuotas tai kad antra kart nedarytu to
	
bufer2isdead:										;jei antras buferis formuojam buf3 ir irasom
	call formbuf
	call writebuf
	
checkbuff2:
	call atnBuf2
	cmp [fbuf2Size], 0
	jle print1
	mov	si, offset fbuf2
	add	si, [fbuf2Size]
		
	xor cx, cx
	jmp Loopas
print1:													 ;jei baigiasi failas o kitas dar ne tai pabaigia spausdint kita
	xor cx, cx
	xor ax, ax
print1loop:
	cmp di, offset fbuf1
	jle print1bufdead
	
	mov al, [di-1]									;di rodo i paskutini neapdirbta
	sub al, '0'
	add al, bl
	mov bl, 0
	push ax
	inc cx
	dec di
	jmp print1loop									;i steka sukisam skaitmenis ir pridedam pernesimus jei yra 

print1bufdead:
	call formbuf										;suformuojam 3 buferi ir irasom i faila
	call writebuf
	call atnBuf1					 
	cmp [fbuf1size], 0
	jle spausdinkliek
	mov di, offset fbuf1
	add di, [fbuf1size]
	
	jmp print1

	
print2:
	xor cx, cx
	xor ax, ax
print2loop:
	cmp si, offset fbuf2
	jle print2bufdead
	
	mov al, [si-1]
	sub al, '0'
	add al, bl
	mov bl, 0
	push ax
	inc cx
	dec si
	jmp print2loop

print2bufdead:
	call formbuf
	call writebuf
	call atnBuf2
	cmp [fbuf2size], 0
	jle spausdinkliek
	mov si, offset fbuf2
	add si, [fbuf2size]
	
	jmp print2
	
spausdinkliek:
	add bl, '0'
	mov ax, [f3h]											;nuseekina i failo pabaiga
	xor cx, cx
	mov dx, 0
	call seek	 
	mov [fbuf3], bl										;i pirma buferio vieta idedam 
	mov ah, 40h
	mov bx, [f3h]
	mov cx, 1
	mov dx, offset fbuf3 
	int 21h	
	ret 
suma endp

formbuf proc																	 ;steke esancius skaitmenis sudeda i buferius. reikia kad stekas butu sutvarkytas ir nustatytas cx
	mov [tmpRegs], ax
	mov [tmpRegs + 2], bx
	mov [tmpRegs + 4], cx
	mov [tmpRegs + 6], dx
	
	pop dx																			 ;issisaugom grizimo adresa, nes jis buna virsuje
	
	mov	[fbuf3size], cx													;nustatom buferio dydi
	mov bx, offset fbuf3												 ;padarom kad bx rodytu i 3 buferio pradzia
copybufloop:
	cmp cx, 0																		;paziurim ar steke turim skaitmenu
	jle copybufloopend
	pop ax																			 ;issipopinam skaitmeni
	
	add al, '0'
	mov [bx], al																 ;istumiam skaitmeni i buferi
	inc bx
	dec cx
	jmp copybufloop
	
copybufloopend:												 
	push dx															;gryzimo ret adresa idedam atgal i steka
	
	mov ax, [tmpRegs]										;visus registrus grazinam atgal
	mov bx, [tmpRegs + 2]
	mov cx, [tmpRegs + 4]
	mov dx, [tmpRegs + 6] 
	ret	
formbuf endp

writebuf proc													;irasom buferi kuri suformavom
	push ax
	push bx
	push cx
	push dx	
	
	cmp [fbuf3size], 0									 ;paziurim ar yra ka spausdint
	jle neraKoSpausdint
	
	mov ax, [pos3]
	mov bx, [pos3 + 2]
	
	mov cx, ax
	sub ax, [fbuf3size]
	mov [pos3], ax
	cmp ax, cx
	jna NoOverflowa
	
	mov cx, bx
	dec bx
	mov [pos3 + 2], bx
	cmp bx, cx
	jna NoOverflowa
	
	mov word ptr[pos3], 1
	mov word ptr[pos3 + 2], 0
	
 NoOverflowa:	
 
	mov dx, [pos3]													 ;nueinam i failo vieta kur reik
	mov cx, [pos3 + 2]
	mov ax, [f3h]
	call seek
	
	mov ah, 40h													;vyksta irasymas
	mov bx, [f3h]
	mov cx, [fbuf3size]
	mov dx, offset fbuf3
	int 21h
	jc	Klaida
	
	mov [fbuf3size], 0									;viska irasem buferio dydis 0
	
neraKoSpausdint: 
	pop dx
	pop cx
	pop bx
	pop ax
	ret
writebuf endp

raskilgesni proc
	push ax
	push bx
	mov ax, [file1size + 2]
	mov bx, [file2size + 2]
	cmp ax, bx
	jb	antrasD
	cmp ax, bx
	ja pirmasD
	
	mov ax, [file1Size]
	mov bx, [file2Size]
	cmp ax, bx
	jb antrasD
	
pirmasD:	
	mov ax, [file1Size]
	mov bx, [file1Size + 2]
	mov [pos3], ax
	mov [pos3 + 2], bx
	jmp raskilgesniend

antrasD:
	mov ax, [file2Size]
	mov bx, [file2Size + 2]
	mov [pos3], ax
	mov [pos3 + 2], bx

raskilgesniend: 
	
	mov ax, [pos3]
	inc ax
	mov [pos3], ax
	cmp ax, 0
	jne oneByteOffsetEnd
	
	mov ax, [pos3 + 2]
	inc ax
	mov [pos3 + 2], ax
	
 oneByteOffsetEnd:
	pop bx		
	pop ax	
	ret
raskilgesni endp

Klaida:
	mov ah, 09h
	mov dx, offset Klaidele1
	int 21h
	mov ah, 4Ch
	int 21h 

;*************************	
;Rezultatu failo sukurimas
;*************************
gaminam proc
	push ax
	push bx
	push cx
	push dx
	mov ah, 03Ch			;i ah irasom dosinio pertraukimo funkcijos numeri 3Ch
	mov cx, 0											
	mov dx, offset f3		;vieta, kur nurodomas failo pavadinimas, pasibaigiantis nuliniu simboliu
	int 21h					;21 pertraukimas, failo sukurimas
	jc klaida				;jei atidarant faila skaitymui neivyksta klaidos, nustatomas carry flag (CF=0), persokam i 'SukurimasAtidarymasRasymui'
		
	mov [f3h], ax			;atmintyje issisaugom rezultatu failo deskriptoriaus numeri 
	pop dx
	pop cx
	pop bx
	pop ax
	ret 
gaminam endp

;*********************************************************** 
;uzdarom failus 
;***********************************************************
	mov ax, [f1h]
	call uzdarytuvas
	mov ax, [f2h]
	call uzdarytuvas
	jmp Pabaiga
	



atidarytuvas proc
	mov dx, ax
	mov al, 0									;leidimas atidaryti faila
	mov ah, 3Dh								;funkcija atidaryti faila
	int 21h										;iskviecia operacine
	jc	Klaida 
	ret
atidarytuvas endp	



uzdarytuvas proc
	push bx
	mov bx, ax
	mov ah, 3Eh								;uzdarysim faila funkcija
	int 21h
	pop bx
	ret
uzdarytuvas endp	


;***********************************************************	
;argumentai:
;***********************************************************
; ax - file handle
; cx:dx - offset from end of file
seek proc																	;pastumia failo zymekli	 
	push ax
	push bx
	push cx
	push dx
	
	mov bx, ax
	mov ah, 42h								 ;seeko funkcija
	mov al, 00h								 ; nuo pradzios baitais skaiciuoja
	int 21h
	jc Klaida
	
	pop dx
	pop cx
	pop bx
	pop ax
	ret
seek endp

;Failo dydis
raskDydi proc		 ;suzinom ilgi failo
	push cx
	push bx
	mov bx, ax
	mov ah, 42h
	mov al, 02h
	xor cx, cx
	xor dx, dx
	int 21h
	jc Klaida
	cmp cx, 0
	jne Klaida
	
	
	pop bx
	pop cx
	ret
raskDydi endp

;nuskaito 1 buferi
atnBuf1 proc
	push ax
	push bx
	push cx
	push dx 
;**********************************************************
;randa pozicija nuo kurios reikia pradet skaityt	
;**********************************************************
	mov dx, [pos1]
	mov bx, [pos1 + 2]
	
	cmp dx, 0																 ;pasiziurim ar netuscias failas
	jne skaiciuokOffseta
	cmp bx, 0
	je ENDfile
		
skaiciuokOffseta:	
	
	mov cx, 10 
	
	mov ax, dx		;atimam buferio dydi
	sub dx, 10
	mov word ptr[pos1], dx
	cmp dx, ax
	jna noOverflow
	
	mov ax, bx			 ;overflow - failas pasibaige
	dec bx
	mov word ptr[pos1 + 2], bx
	cmp bx, ax
	jna noOverflow

	mov cx, dx
	add cx, 10
	mov word ptr[pos1], 0
	mov word ptr[pos1 + 2], 0

	
noOverflow:
	push cx
	
	mov dx, [pos1]								;paseekinam i ta pozicija kuria paskaiciavom
	mov cx, [pos1 + 2]
	mov ax, [f1h]
	call seek
	
	pop cx
	
	mov al, cl						;iskviecia DOS'a kad nuskaitytu duomenis
	mov ah, 3fh
	mov bx, [f1h]
	mov dx, offset fbuf1
	int 21h
		
	mov [fbuf1size], ax			 ;irasom dydi ir gryztam
	
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	
ENDfile:							;baigesi failas tai irasom 0
	mov [fbuf1size], 0
	pop dx
	pop cx
	pop bx
	pop ax
	ret
 atnBuf1 endp
;nuskaito antra buferi	
atnBuf2 proc
	push ax
	push bx
	push cx
	push dx 
;**********************************************************
;randa pozicija nuo kurios reikia pradet skaityt	
;**********************************************************
	mov dx, [pos2]
	mov bx, [pos2 + 2]
	
	cmp dx, 0
	jne skaiciuokOffseta2
	cmp bx, 0
	je ENDfile2
		
skaiciuokOffseta2:	
	
	mov cx, 10 
	
	mov ax, dx		;atimam buferio dydi
	sub dx, 10
	mov word ptr[pos2], dx
	cmp dx, ax
	jna noOverflow2
	
	mov ax, bx			 ;overflow - failas pasibaige
	dec bx
	mov word ptr[pos2 + 2], bx
	cmp bx, ax
	jna noOverflow2

	mov cx, dx
	add cx, 10
	mov word ptr[pos2], 0
	mov word ptr[pos2 + 2], 0

	
noOverflow2:
	push cx
	
	mov dx, [pos2]
	mov cx, [pos2 + 2]
	mov ax, [f2h]
	call seek
	
	pop cx
	mov ah, 3fh
	mov bx, [f2h]
	mov dx, offset fbuf2
	int 21h
	
	mov [fbuf2size], ax
	
	pop dx
	pop cx
	pop bx
	pop ax
	ret

ENDfile2:
	mov [fbuf2size], 0
	pop dx
	pop cx
	pop bx
	pop ax
	ret
 atnBuf2 endp

;-----------------------------------------------------------------------
printnum proc
	push ax
	push bx
	push cx
	push dx
	mov bx, 10
	xor cx, cx	 
printnumloop:
	xor dx, dx 
	div bx										;ax dalinasi is bx
	add dx, '0'									;is paprasto skaitmeni pavercia ascii kodu
	push dx										;tik vieno baito tas ascii
	inc cx
	cmp ax, 0
	ja printnumloop

printloop:
	mov ah, 02h
	pop dx
	int 21h
	dec cx 
	cmp cx, 0
	ja	printloop
	pop dx
	pop cx
	pop bx
	pop ax
	ret
printnum endp 

spausdink proc
	push ax
	mov ah, 09h
	int 21h
	pop ax
	ret
spausdink endp
END	 
	
