; 7. Para�ykite programa, kuri duotai simboliu eilutei ir simboliams S1 ir S2
; pakeicia eiluteje simbolius S1 simboliu S2. Pvz., S1=�a�, S2=�N�,
; eilutei abdv aahdN turi atspausdinti Nbdv NNhdN.

.model small
.stack 100h
.data
	ivesk		db 'ivesk eilute: $'
	buffdydis	db 121
	kiek		db ?
	buffer		db 121 dup(?)
	S1 			db ' '
;	S2			db 'N'
	nl			db 10,13,'$' 	;10 - n.e., 13 - nuo pradzios
	
	
.code

START:
	mov ax, @data
	mov ds, ax
;isvedimas
	mov ah, 9					;spausdinimo k. - vis1 eilut3 spausdina
	mov dx, offset ivesk 		;ji turi b8ti dx registre
	int 	21h 				;iveda komanda
;skaitymas
	mov ah, 0Ah					;skaito ivedama eilute
	mov dx, offset buffdydis 	;i DX registra
	int		21h					;iveda komanda vykdymui
;isvesk nauja eilute
	mov ah, 9
	mov dx, offset nl			;tuscia eil.
	int		21h
;pasiruo�imas ciklui
	xor cx, cx					;nunulinamas cx registras
	xor bx, bx
	mov cl, [kiek]
	mov si, offset buffer		;si naud lodsb f-joj, kad perkelt po 1 b i AL registra
loopas:
	lodsb						;is SI i AL
	inc bx			;padidinam zodzio simboliu skaitikli
	cmp cl, 0                   ;ar likusiu simb skaicius lygus 0?
	je exit1						;jump if equal
	cmp al, [S1]                ;ar simbolis lygus S1
	je isvesk					;jump if eq
grizk:
	dec cl						;maz ciklo skaitiklis
	jmp loopas
isvesk:
	xor dx, dx					;else dx naik
	mov dl, bx;DEL SITO nesu tikras - TIKRIAUSIAI kazkaip kitaip registro reiksme isvest reikia
	mov ah, 02h	;isvest 1 simboli dos'ui
	int 21h
	xor bx, bx;nunulinam bx
	jmp grizk
exit1:
	xor dx, dx					;else dx naik
	mov dl, bx;DEL SITO nesu tikras - TIKRIAUSIAI kazkaip kitaip registro reiksme isvest reikia
	mov ah, 02h	;isvest 1 simboli dos'ui
	int 21h
	xor bx, bx;nunulinam bx
	jmp grizk
exit:
	mov ah, 4ch					;16ainej
	int		21h
	ret							;grazina valdyma

end start
	
	
	
	
	
