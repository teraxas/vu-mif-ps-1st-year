program masyv;
 
const 	Mdydis = 5;

type masyvas=array [1..Mdydis] of integer;

var N,min,max,i:integer;					//N-kiekis, min/max skaiciai
	mas:masyvas;							//bendras skaiciu masyvas

begin
	Writeln('Įvesk sveiką skaičių N, tenkinantį sąlygas: N>1, N<=',Mdydis);
	Readln(N);
	If (N>1) and (N<=Mdydis)  then
								begin
									//Įvedimas
									Writeln ('įvesk ',N,' sveikų skaičių seką');  
									for i:=1 to N do Readln(mas[i]);
									
									// min/max radimas
									min:=mas[1]; max:=mas[1];
									for i:=1 to N do
									begin
										If mas[i]<min then min:=mas[i]
													  else If mas[i]>max then max:=mas[i];
										
									end;
									
									// Išvedimas
									Writeln('[',(min),' ; ',(min+(max-min) div 3),']');
									for i:=1 to N do	
										If (mas[i]>=min) and (mas[i]<=(min+(max-min) div 3)) then Write(mas[i]:6);														
									Writeln;
									Writeln('[',(min+(max-min) div 3),' ; ',(min+(max-min)*2 div 3),']');
									for i:=1 to N do
										If (mas[i]>=(min+(max-min) div 3)) and (mas[i]<=(min+(max-min)*2 div 3)) then Write(mas[i]:6);
									Writeln;
									Writeln('[',(min+(max-min)*2 div 3),' ; ',(max),']');
									for i:=1 to N do
										If (mas[i]>=(min+(max-min)*2 div 3)) and (mas[i]<=max) then Write(mas[i]:6);	
								end
							else Writeln('Netinkamas skaičius. Turi būti N>1, N<=',Mdydis);	
end.

