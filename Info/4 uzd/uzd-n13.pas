{uzd
	n13:
	* sud. dvip. s.
	* proc. įterpia prieš reikšme nurodytą el. naują elementą
	* jei tokio el. nėra - išveda atitinkamą pranešimą 
}
program n13;

const 	ZinKiekis 	= 'Įvesk elementų skaičių';
		ZinVesk		= 'Vesk elementus';
		ZinNerastas	= 'Tokio elemento rasti nepavyko';
		ZinPries	= 'Prieš kurį el. įterpti?';
		ZinIterp	= 'Ką įterpti?';

type	SarTipas 	= ^ElemTipas;
		ElemTipas	= record
			pries,po:SarTipas;
			duom:string;
			end;


	
procedure DSpridek(var sar:SarTipas;d:string);
var nj:SarTipas;
begin
	new(nj);
	nj^.duom:=d;
	nj^.pries:=nil;
	If sar<>nil 
		then begin
			nj^.po:=sar;
			sar^.pries:=nj;	
			end
		else nj^.po:=nil;
	sar:=nj;
end;

procedure DSspausdink(sar:SarTipas);
begin
	writeln;
	writeln('Sąrašas:');

	while sar<>nil do
		begin
			writeln(sar^.duom);
			sar:=sar^.po;
		end;
end;


procedure DSiterpk(iterp,PriesKa:string;var sar:SarTipas; var rado:boolean);
var	naujas, sar2:SarTipas;
	
begin
	rado:=false;
	sar2:=sar;
	
	while (sar2<>nil) and (rado=false) do
		begin
			If sar2^.duom = PriesKa then 
				begin
					New(naujas);
					naujas^.duom:=iterp;
					naujas^.pries:=sar2^.pries;
					naujas^.po:=sar2;
					If sar2^.pries <> nil 	then sar2^.pries^.po:=naujas
										else sar:=naujas;
					sar2^.pries:=naujas;
					rado:=true;
				end;
			sar2:=sar2^.po;
		end;
end;



var	sar:SarTipas;
	kiek,i:integer;
	elem,kur:string;
	yra:boolean;
BEGIN
sar:=nil;

writeln(ZinKiekis);
readln(kiek);

//el. įvedimas
Writeln(ZinVesk);
For i:=1 to kiek do
	begin
		readln(elem);		//nusk. elem.
		DSpridek(sar,elem);
	end;
DSspausdink(sar);

//kur ir ką įterpt?
writeln;
writeln(ZinPries);
readln(kur);
writeln(ZinIterp);
readln(elem);

//įterpk!
DSiterpk(elem,kur,sar,yra);
If yra = false then writeln(ZinNerastas)
				else DSspausdink(sar);
	
END.

