program uzd;

const 	MAXeilutes	= 10;
		MAXzodziai	= 100;
		S1			= 2; 							//pirmo simbolio nr.
		S2			= 3;							//antro simbolio nr. (nuo galo)

type 	zodMAS = array [1..MAXzodziai] 	of string;		//NEREIKIA masyvo

procedure DUtrys(zodis:string; var rez:text);
begin
	If zodis[S1]=zodis[length(zodis)-(S2-1)] then writeln (rez,zodis);
end;

var 	duom,rez:string;
		fl1,fl2:text;
		Zmas:zodMAS;								//Žodžių masyvas
		eil:string;									//eilutė
		kl1,										//IOResult reikšmės
		i,j,z:integer;								//z - žodžių skaitiklis
	

BEGIN

repeat
	writeln('Įvesk duomenų failo vardą');
	Readln(duom); 
	Assign(fl1,duom);
	{$I-}
	reset(fl1);
	{$I+}
	kl1:=IOResult;
	If kl1=0 then
				begin
					writeln('Įvesk rezultatų failo vardą');
					readln(rez);
							
					Assign(fl2,rez);
					{$I-}
					reset(fl2);		//Vienodumo?
					rewrite(fl2);
					{$I+} 
											
					If (IOResult = 0) 	then
											begin
												z:=1;
												For i:=1 to MAXeilutes do
													begin
														readln(fl1,eil);
														For j:=1 to length(eil) do
															begin
																If (eil[j]<>' ') 	then 
																						begin
																				Zmas[z]:=Zmas[z] + eil[j];
																				If j=length(eil) then z:=z+1;
																						end
																					else
																				z:=z+1;
															end;
													end;
												
												//vienodų simbolių radimas ir įrašymas į failą	
												For i:=1 to z do DUtrys(Zmas[i],fl2);
													
											end
										else
											begin
												writeln('Rezultatų failo "',rez,'" sukurti neįmanoma');
												close(fl1);
												halt;
											end;	
				end
			else writeln('Duomenų failas "',duom,'" nerastas');
until kl1 = 0;
	close(fl1);
	close(fl2);
END.
