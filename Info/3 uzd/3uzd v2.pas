program uzd;
uses crt;
const 	S1			= 2; 							//pirmo simbolio nr.
		S2			= 3;							//antro simbolio nr. (nuo galo)




function tikrink(eilute:string):string;

	function zodtikr(zodis:string):boolean;
	var	simbolis1,simbolis2:char;
	begin
		zodtikr:=false;
		If  length(zodis)>2 then
								begin
									simbolis1:=zodis[S1];
									simbolis2:=zodis[length(zodis)-(S2-1)];
									If simbolis1=simbolis2 	
										then zodtikr:=true;
								end;
	end;
	
var zod,ats:string;
	j:integer;
begin
	zod:='';	ats:='';
	For j:=1 to length(eilute) do
				begin
					If eilute[j]<>' ' 	then zod:=zod+eilute[j]
										else
											If zodtikr(zod) 	
												then begin
														ats:=ats+zod+(' ');
														zod:='';
													 end;						 
				end;
	If zodtikr(zod) 	
		then begin
				ats:=ats+zod+(' ');
				zod:='';										
			 end;										
	tikrink:=ats;
end;

var 	vardas:string;								//failo vardas
		duom,rez:text;								
		eil:string;									//eilutė
		kl:integer;									//IOResult reikšmė

BEGIN
	textbackground(blue);
	textcolor(white);
	repeat 								// DUOM
		writeln('Įvesk duomenų failo vardą');
		readln(vardas); 
		assign(duom,vardas);
		{$I-}
		reset(duom);
		{$I+}
		kl:=IOResult;
		If kl<>0 then writeln('Duomenų failo "',vardas,'" atidaryti nepavyko');
	until kl=0;
	
	repeat								// REZ
		writeln('Įvesk rezultatų failo vardą');
		readln(vardas);
		assign(rez,vardas);
		{$I-}			
		rewrite(rez);		
		{$I+}
		kl:=IOResult;	
		If kl<>0 then writeln('Rezultatų failo "',vardas,'" sukurti neįmanoma'); 
	until kl=0;
	
	while not eof(duom) do
		begin
			readln(duom,eil);
			writeln(rez,tikrink(eil));
		end;
		
	
	close(duom);
	close(rez);
	
END.

