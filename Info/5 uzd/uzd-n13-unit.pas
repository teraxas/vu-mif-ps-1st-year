{uzd
	n13:
	* sud. dvip. s.
	* proc. įterpia prieš reikšme nurodytą el. naują elementą
	* jei tokio el. nėra - išveda atitinkamą pranešimą 
}
program n13;
		
uses DSunit;

const 	ZinKiekis 	= 'Įvesk elementų skaičių';
		ZinVesk		= 'Vesk elementus';
		ZinNerastas	= 'Tokio elemento rasti nepavyko';
		ZinPries	= 'Prieš kurį el. įterpti?';
		ZinIterp	= 'Ką įterpti?';


		
var	sar:SarTipas;
	kiek,i:integer;
	elem,kur:string;
	yra:boolean;	
	
procedure DSpridek(var sar:SarTipas;d:string);
var nj:SarTipas;
begin
	new(nj);
	nj^.duom:=d;
	nj^.pries:=nil;
	If sar<>nil 
		then begin
			nj^.po:=sar;
			sar^.pries:=nj;	
			end
		else nj^.po:=nil;
	sar:=nj;
end;
	
	
BEGIN
sar:=nil;

writeln(ZinKiekis);
readln(kiek);

//el. įvedimas
Writeln(ZinVesk);
For i:=1 to kiek do
	begin
		readln(elem);		//nusk. elem.
		DSpridek(sar,elem);
	end;
DSspausdink(sar);

//kur ir ką įterpt?
writeln;
writeln(ZinPries);
readln(kur);
writeln(ZinIterp);
readln(elem);

//įterpk!
DSiterpk(elem,kur,sar,yra);
If yra = false then writeln(ZinNerastas)
				else DSspausdink(sar);
	
END.

