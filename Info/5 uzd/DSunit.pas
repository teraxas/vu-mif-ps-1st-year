unit DSunit;

interface
type	SarTipas 	= ^ElemTipas;
		ElemTipas	= record
			pries,po:SarTipas;
			duom:string;
			end;
			
	procedure DSpridek(var sar:SarTipas;d:string);
	procedure DSspausdink(sar:SarTipas);
	procedure DSiterpk(iterp,PriesKa:string;var sar:SarTipas; var rado:boolean);
	
	

//----------------------------------------------------------------------	
implementation


procedure DSpridek(var sar:SarTipas;d:string);
var nj:SarTipas;
begin
	new(nj);
	nj^.duom:=d;
	nj^.pries:=nil;
	If sar<>nil 
		then begin
			nj^.po:=sar;
			sar^.pries:=nj;	
			end
		else nj^.po:=nil;
	sar:=nj;
end;

procedure DSspausdink(sar:SarTipas);
begin
	writeln;
	writeln('Sąrašas:');

	while sar<>nil do
		begin
			writeln(sar^.duom);
			sar:=sar^.po;
		end;
end;


procedure DSiterpk(iterp,PriesKa:string;var sar:SarTipas; var rado:boolean);
var	naujas, sar2:SarTipas;
	
begin
	rado:=false;
	sar2:=sar;
	
	while (sar2<>nil) and (rado=false) do
		begin
			If sar2^.duom = PriesKa then 
				begin
					New(naujas);
					naujas^.duom:=iterp;
					naujas^.pries:=sar2^.pries;
					naujas^.po:=sar2;
					If sar2^.pries <> nil 	then sar2^.pries^.po:=naujas
										else sar:=naujas;
					sar2^.pries:=naujas;
					rado:=true;
				end;
			sar2:=sar2^.po;
		end;
end;

end.
