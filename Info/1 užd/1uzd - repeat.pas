program seka2;
var x,xx:integer;			// skaičius ir buvęs skaičius
	neig,teig,nul:integer;	// neig./teig./nulinių skaičių kiekiai

BEGIN
	neig:=0; nul:=0; teig:=0;
	
	writeln('Veskite seką');
	readln(xx);
	If xx<0 then neig:=neig+1
		else If xx=0 then nul:=nul+1
				else  teig:=teig+1;
	x:=xx;
	
	repeat
		begin
			xx:=x;
			readln(x);
			If x<0 then neig:=neig+1
				else If x=0 then nul:=nul+1
						else  teig:=teig+1;
		end;
	until x=xx;
	
	Writeln('Neigiami: ',neig);
	Writeln('Nuliniai: ',nul);
	Writeln('Teigiami: ',teig);
END.

