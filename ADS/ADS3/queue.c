/*	--- ADT-EILE ---
 * Karolis Jocevičius, PS1
 * 2012
*/
//stasys.peldzius@mif.vu.lt

int qFault;
#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

//Sukuria tuscia eile
que *qNew()
{
	que *q = malloc(sizeof(*q));
	
	if (q==NULL) 
		return NULL;
	
	q->fr = NULL;
	q->bk = NULL;
	q->cnt = 0;
	
	
	qFault = 0;
	return q;
}

//Prideda nauja elementa prie eiles
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
que *qEnq(que *q, void *x){
	qFault = 0;
	if (q == NULL){
		qFault = 1;
		return q;
	}
	if (qCHKf(q) == 1){
		qFault = 3;
		return q;
	}
	qElem *new = malloc(sizeof(*new));
	new->prv = NULL;
	new->dat = x;
	if (q->bk != NULL)
		q->bk->prv = new;
	q->bk = new;
	if (q->fr == NULL)
		q->fr = new;
	
	q->cnt++;
	return q;
}

//Nuskaito pirma eiles elementa
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
void *qRead(que *q){
	qFault=0;
	if (q == NULL){
		qFault=1;
		return NULL;
	}
	if (q->cnt == 0){
		qFault=2;
		return NULL;
	}
	void* ret;
	ret = q->fr->dat;
	return ret;
}

//Sunaikina pirma elementa
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
que *qDeq(que *q){
	qFault=0;
	if (q == NULL){
		qFault=1;
		return NULL;
	}
	if (q->cnt == 0){
		qFault=2;
		return NULL;
	}
	qElem *tmp;
	tmp = q->fr;
	q->fr = q->fr->prv;
	q->cnt--;
//	tmp->dat = NULL;
	free(tmp);
	if (tmp != NULL)
		qFault = 4;
	return q;
}

//Perkelia elementą atgal į eilės galą
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
que *qReq(que *q){
		qFault=0;
	if (q == NULL){
		qFault=1;
		return NULL;
	}
	if (q->cnt == 0){
		qFault=2;
		return NULL;
	}
	void *tmp = qRead(q);
//	tmp = q->fr->dat;
	qTake(q);
	qEnq(q,tmp);
	return q;
}

//i6ima elementa, bet jo nesunaikina
que *qTake(que *q){
		qFault=0;
	if (q == NULL){
		qFault=1;
		return NULL;
	}
	if (q->cnt == 0){
		qFault=2;
		return NULL;
	}
	q->fr = q->fr->prv;
	q->cnt--;
	return q;
}

//Sunaikina eile
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
que *qFree(que *q){
	if (q == NULL)
		qFault=1;
	if (q->cnt == 0)
		qFault=2;

	do {
		qDeq(q);
		} while (q->cnt != 0);
	free(q);
	return 0;
}

//Kiek elementu yra eileje?
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
int qCount(que *q){
	if (q == NULL){
		qFault=1;
		return 0;
	}
	int a = q->cnt;
	return a;
}

//Ar tuscia?
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
int qCHKe(que *q){
	if (q == NULL){
		qFault=1;
		return 1;
	}
	int r = 0;
	int a=q->cnt;
	if (a == 0) 
		r = 1;
	return r;
}

//Ar pilna?
// !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!
int qCHKf(que *q){
	return 0;
}

//qFault check
int qFchk(){
	return qFault;
}
