/*	--- ADT-EILE ---
 * Karolis Jocevičius, PS1
 * 2012
*/


//----------------------------------------------------------------------
//		TIPAI
//----------------------------------------------------------------------

//Elemento tipas
typedef struct qElem {
	struct qElem *prv;			//nuoroda į prieš el. esantį elementą
	void *dat;					//duomenys (void pointer)
}qElem;

//Eiles tipas
typedef struct que {
	qElem *fr,*bk;				//Nuorodos į eilės priekį ir galą el.
	int cnt;					//elementų skaitiklis
}que;



//----------------------------------------------------------------------
//		FUNKCIJOS
//----------------------------------------------------------------------

/* qNew - sukuria tuscia eile
 * Pvz. *queue = qNew();*/
que *qNew();

//----------------------------------------------------------------------
/* qEnq - prideda nauja elementa i eile
 * Pvz. *queue = qEnq(*queue,*duomenys);
 * Paduodamas pointeris į eilę ir pointeris į naujus duomenis.
 * Į eilės galą pridedamas elementas su pointeriu i naujus duomenis
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!! */
que *qEnq(que *q, void *x);

//----------------------------------------------------------------------
/* qRead - nuskaito eiles pirmaji (priekini) elementa
 * Pvz. *x = qRead(*queue);
 * qRead reiksme - pointeris i elemento duomenis 
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!*/
void *qRead(que *q);

//----------------------------------------------------------------------
/* qDeq - isima pirmaji elementa is eiles
 * Pvz. *queue = *qDeq(*queue); 
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!*/
que *qDeq(que *q);

//----------------------------------------------------------------------
/* qReq - grazina elementa i eiles gala
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!*/
que *qReq(que *q);

//----------------------------------------------------------------------
/* qTake - isima elementa, bet jo nesunaikina */
que *qTake(que *q);

//----------------------------------------------------------------------
/* qFree - sunaikina eile
 * Pvz. qFree(*queue); 
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!*/
que *qFree(que *q);

//----------------------------------------------------------------------
/* qCount - nuskaito elementu skaitiklio duomenis
 * t.y. queue->cnt
 * Pvz. x = qCount;
 * Kai: int x, qCount 
 * !!!!!! Eile TURI BUTI SUKURTA (qNew) !!!!!!*/
int qCount(que *q);

//----------------------------------------------------------------------
/* qCHKe - patikrina ar eile tuscia
 * Pvz. x = qCHKe(*queue);
 * Kai: int x, qCHKe
 * qCHKe = 	1 - eile tuscia ARBA lygi "NULL"
 * 			0 - eile netuscia */
int qCHKe(que *q);

//----------------------------------------------------------------------
/* qCHKf - patikrina ar eile pilna
 * Pvz. x = qCHKf(*queue);
 * Kai: int x, qCHKe
 * qCHKe = 	1 - eile pilna
 * 			0 - eile nepilna */
int qCHKf(que *q);

/* Paskutines komandos klaidu tikrinimas
 * 1 - eile = NULL
 * 2 - eile tuscia
 * 3 - eile pilna*/
int qFchk();

