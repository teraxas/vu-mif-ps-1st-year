//Psycho by Karolis Jocevi2ius

//Parameters
int days,GrT,InT, GrK, InK, GRsize,freq,p1,p2,h;

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "queue.h"
#include "SmiciausIlgasSveikasSk.h"
#include "pacientas.h"

/*
	gcc -o psycho main.c queue.c SmiciausIlgasSveikasSk.c pacientas.c

	./psycho in.dat out.txt
*/

//======================================================================
int ReadFile(FILE *f){
	fscanf(f,"days %d\n",&days);
	fscanf(f,"GrT %d\n",&GrT);
	fscanf(f,"InT %d\n",&InT);
	fscanf(f,"GrK %d\n",&GrK);
	fscanf(f,"InK %d\n",&InK);
	fscanf(f,"GRsize %d\n",&GRsize);
	fscanf(f,"freq %d\n",&freq);
	fscanf(f,"p1 %d\n",&p1);
	fscanf(f,"p2 %d\n",&p2);
	fscanf(f,"h %d\n",&h);
	return 0;
}

void PrintParam(){
	printf("\n\n");
	printf("======================================================================\n");
	printf("Dienos: %d / Valandos (9 darbo valandos dienoje): %d\n",days,(days*9));
	printf("Vizito ilgis ir kaina:\n 1. Group: %dh kainuoja %d\n 2. Individual: %dh kainuoja %d\n",GrT,GrK,InT,InK);
	printf("Pacientas grizta kas %d valandas\n",freq);
	printf("Tikimybe, kad ateis naujas pac. %d proc\n",p1);
	printf("Tikimybe, kad grupes sesijos pacientas pasveiks %d proc greiciau \nnei individualus yra %d proc\n",h,p2);
	printf("======================================================================\n");
	printf("\n");
}

void PrintStatus(que *mainQ, que *curvQ, que *grQ){
	printf("\n   STATUS: ");
	printf("mainQ: %d , ",qCount(mainQ));
	printf("new gr queue: %d , ",qCount(grQ));
	printf("cur. visits queue: %d ;",qCount(curvQ));
}

void PrintPat(patient *p){
	printf("\nPAT: type: %d, ",p->type);
	printf("hp: %d, ",p->health);
	printf("visits: %d, ",p->cnt);
	printf("spent: %d, ",p->sum);
	printf("vProg: %d, ",p->vProg);
	printf("nxt: %d ;",p->nxt);
}


//===MAIN===============================================================
int main(int argc, char **argv)
{
//===VARS===
	que *mainQ,*qNewGr, *qCurVisits;
	int i,j,h;
	patient *nPat, *cur;
	patient *tmp;
	
	//is viso valandu vizitu
	int InCntH=0, GrCntH=0;
	
	//is viso vizitu
	int InCntV = 0, GrCntV = 0;
	
	//sumokėti pinigai
	int InSum = 0, GrSum = 0;
	

//===TEST VARS===
	int ngrCnt=0;
	int nPatCountINT = 0;

//Time rand
srand(time(NULL));

//===FILES===
	if ( argc != 3 ) /* argc should be 3 for correct execution */
	{
		printf("\n\nKlaidingai ivesti parametrai!!!\n");
		printf( "naudojimas: %s input output \n", argv[0] );
		return -1;
	}
	// argv[1] is a filename to open
	FILE *inp = fopen( argv[1], "r" );
	FILE *out = fopen( argv[2], "w" );
	// fopen returns 0 on failure
	if ( inp == 0 || out == 0){
		printf( "Could not open file\n" );
		return -1;
	}
//===READ===
	ReadFile(inp);
	PrintParam();
//===QUEUES===
	mainQ = qNew();//visu pacientu eile
	qNewGr = qNew();//naujos grupes formavimo eile
	qCurVisits = qNew();//dabar vykstanciu vizitu eile

//===Ilgas Sveikas===
/*	Sk *nPatCount, *vienasSk, *rezSk;
	char nulis[2] = "0 ";
	char vienas[2] = "1 ";
	SukurtiSk(&nPatCount);
	SukurtiSk(&vienasSk);
	PriskirtiSk(&nPatCount,nulis);
	PriskirtiSk(&vienasSk,vienas);*/
	
//===MAIN===
	//kiekviena valanda
	for(i=0;i!=(days*9);i++){	//viena diena - 9 h
		printf("\n\n====HOUR %d==========",i);
		PrintStatus(mainQ,qCurVisits,qNewGr);
		//ar ateis naujas pacientas?
		if (rNum(1,100)<(p1+1)){
			nPat = pNew(nPat);
			
			nPatCountINT++;//int
			
		//	SudetiSk(nPatCount,vienasSk,&rezSk);//Ilgas sveikas counteris
			
		/*	SudetiSk(nPatCount,vienasSk,&rezSk);//Ilgas sveikas counteris
			PanaikintiSk(&nPatCount);
			nPatCount = rezSk;*/
			
			printf("\nNAUJAS pat: Type: %d",nPat->type);
			if (nPat->type == 1)//jei individualus - tiesiai i vykstanciu eile
				qCurVisits=qEnq(qCurVisits,nPat);//pridedam prie vykstanciu vizitu
			if (nPat->type == 0) 
				qNewGr=qEnq(qNewGr,nPat);
			PrintStatus(mainQ,qCurVisits,qNewGr);
		}
		
		//ar nauja grupe pilna?
		if (qCount(qNewGr) == GRsize){
			ngrCnt++;
			
			printf("\nGroup formed!!!");
			
			while (qCHKe(qNewGr)!=1){	//kol eile netuscia
				tmp = qRead(qNewGr);
				qTake(qNewGr);
				qEnq(qCurVisits,tmp);//pridedam prie vykstanciu vizitu
			//	printf("\ntestENQ	NEW GROUP\n");//TEST
			}
			PrintStatus(mainQ,qCurVisits,qNewGr);
		}
		
		
		//vykstanciu vizitu eiles sudarymas
		if (qCount(mainQ)>0){
			printf("\n Checking mainQ... ");
			cur = qRead(mainQ);
			while (cur != NULL){
				if (cur->nxt == i && qCHKe(mainQ) != 1){
					qTake(mainQ);
					qEnq(qCurVisits,cur);
					printf("\n   **Visitor came back");
					cur = qRead(mainQ);
				}
				else cur = NULL;
			}
			printf("\n Checking mainQ: DONE");
			PrintStatus(mainQ,qCurVisits,qNewGr);
		}
		

		//vizitu eiles vykdymas
			//cur visits NOT empty 
		if (qCHKe(qCurVisits) == 0){
			int visits = qCount(qCurVisits);
			printf("\n This hour: %d visits",visits);
			for(j = 0 ; j != visits ; j++){
				//get visitor, inc progress
				cur = qRead(qCurVisits);
				cur->vProg++;
				printf("\n------------");
				PrintPat(cur);
				//if visit is done
				if (cur->type == 1){
					InCntH++;
					if (cur->vProg == InT){
						InCntV++;
						pInVisit(cur,InK,freq,i);
					}
				}	
				if (cur->type == 0){
					GrCntH++;
					if (cur->vProg == GrT){
						GrCntV++;
						pGrVisit(cur,GrK,p2,h,freq,i);
					}
				}
				//visit ended
				if (cur->vProg == 0){
					PrintPat(cur);
					printf("\n   ***visit SUCCESSFUL!!!");
					//Pasveikooooo!!!!! :)
					if (cur->health > 99){
						if (cur->type == 1)
							InSum = InSum + cur->sum;
						if (cur->type == 0)
							GrSum = GrSum + cur->sum;
						PrintStatus(mainQ,qCurVisits,qNewGr);//T
						qCurVisits = qDeq(qCurVisits);
						cur = NULL;
						if (cur == NULL)
							printf("\n      *removed*");
							else 
							printf("\n    *FAIL remove - %d*",qFchk());
						PrintStatus(mainQ,qCurVisits,qNewGr);	
					}
					//Nepasveiko :(
					if (cur != NULL)
						if (cur->health < 100){
							qTake(qCurVisits);
							qEnq(mainQ,cur);
						}
				}
				//visit continues...
				if (cur != NULL)
					if (cur->vProg != 0){
						qReq(qCurVisits);
						printf("\n   ***visit continues...");
					}
			}	
		}	
	}
	
//===CHECKS===
	
	int apxIn = InSum / InCntV;
	int apxGr = GrSum / GrCntV;
	
	float VapxIn = (float)InCntH/(days*9);
	float VapxGr = (float)GrCntH/(days*9);
	
//===END===
	fclose(inp);
	fclose(out);
	
	qFree(mainQ);
	qFree(qCurVisits);
	qFree(qNewGr);	
	
//===END PRINTS===
	PrintParam();
	printf("\n\n=====================================================");
	printf("\nPacientai (ILGAS SVEIKAS): ");
	//	ParasytiSk(nPatCount);
	printf("\nPacientai (int): %d\n",nPatCountINT);
	printf("Grupes: %d\n", ngrCnt);
	printf("Ind. vizitu valandos: %d, Grp. vizitu valandos: %d\n",InCntH,GrCntH/GRsize);
	printf("Per valanda vidutiniskai apsilanko:\n  %f ind. klientu,\n  %f gr klientu\n\n",VapxIn,VapxGr);
	printf("Vidutiniskai individualus lankytojai isleidzia %d\n",apxIn);
	printf("Vidutiniskai grupiu lankytojai isleidzia %d\n", apxGr);
	printf("=====================================================\n");
	
	printf("\nFinansiskai labiau verta naudotis ");
	if (apxIn>apxGr)
		printf("grupinio gydymo paslauga\n");
		else
			printf("individualaus gydymo paslauga\n");
	printf("\nGydimo istaigai verta samdyti\n * %d individualiu\n * %d grupiniu\n     uzsiemimu specialistu\n",InCntV/(days*9)+1,((GrCntV/(days*9))/GRsize)+1);
	printf("=====================================================\n\n");
	return 0;
}

