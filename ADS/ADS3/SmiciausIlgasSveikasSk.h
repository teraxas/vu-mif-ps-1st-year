#ifndef INCLUDE_SMICIAUSILGASSVEIKASSK
#define INCLUDE_SMICIAUSILGASSVEIKASSK
typedef struct Sk Sk;
struct Sk{/*Ant 32 bitų architekturos Sk užima baitų=5+SkaitmenuSk*/
	char *JauniausiasSkaitmuo;/*Rodyklė į jauniausią skaitmenį (į char masyvą). Char reikšmės intervale [0..9]*/
	unsigned int SkaitmenuSk;/*Maksimali skaitmenų masyvo indekso reikšmė (skaitmenų yra SkaitmenuSk+1)*/
	char SF;/*Sign Flag (Ženklo vėliava). Jei SF=0, tai skaičius teigiamas, kitais atvejais neigiamas*/
};
int SukurtiSk(Sk **Skaicius);/*Sukuria skaičių (išsiskiria atmintį). Return klaidos kodą*/
int PanaikintiSk(Sk **Skaicius);/*Ištrina skaičių (atlaisvina atmintį). Return klaidos kodą*/
int NuskaitytiSk(Sk *Skaicius);/*Nuskaito skaičių (leidžia įvesti). Return klaidos kodą*/
int PriskirtiSk(Sk **Skaicius,char *KonstantosStr);/*Skaicius priskiria KonstantosStr. Return klaidos kodą (Jei pirmas simbolis 0, tai visas skaičius bus priskirtas 0. Skaito KonstantosStr iki pirmo neskaičiaus)*/
int PrilygintiSk(Sk **Skaicius,Sk *Skaicius2);/*Skaicius prilygina Skaicius2. Return klaidos kodą*/
int SudetiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3);/*Prie Skaicius prideda Skaicius2. Rezultatą įrašo į Skaicius3. Return klaidos kodą (Pastaba: Skaicius3 galima nurodyti ir ten pat, kur rodo Skaicius arba Skaicius2)*/
int AtimtiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3);/*Iš Skaicius atima Skaicius2. Rezultatą įrašo į Skaicius3. Return klaidos kodą (Pastaba: Skaicius3 galima nurodyti ir ten pat, kur rodo Skaicius arba Skaicius2)*/
int PadaugintiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3);/*Skaicius padaugina iš Skaicius2. Rezultatą įrašo į Skaicius3. Return klaidos kodą (Pastaba: Skaicius3 galima nurodyti ir ten pat, kur rodo Skaicius arba Skaicius2)*/
int PadalintiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Div,Sk **Mod);/*Skaicius dalina iš Skaicius2. Sveikąją dalį įrašo į Div, trupmeninę į Mod. Return klaidos kodą (Pastaba: Div ir Mod galima nurodyti ir ten pat, kur rodo Skaicius arba Skaicius2)*/
int PalygintiSk(Sk *Skaicius,Sk *Skaicius2);/*Palygina skaičius ir, jei skaičiai lygūs, return 0, jei Skaicius>Skaicius2, return 1, jei Skaicius<Skaicius2, return 2. Klaidos atveju gali return 3 arba 4*/
int ParasytiSk(Sk *Skaicius);/*Parašo skaičių ekrane. Return klaidos kodą*/
void AtspausdintiKlaida(int IOResult);/*Jei IOResult!=0, atspausdina ekrane klaidą pagal IOResult numerį*/
/*Klaidų grąžinimai:
0-Klaidų nėra;
1-Įvestas blogas skaičiaus formatas;
2-Nėra laisvos atminties;
3-Skaičius nesukurtas (pirma reik panaudoti funkciją SukurtiSk);
4-Skaičius dar neturi jokios reikšmės (pirma reik panaudoti funkciją NuskaitytiSk);
5-Dalyba iš 0;
*/
#endif
