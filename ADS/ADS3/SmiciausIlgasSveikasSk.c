#include "SmiciausIlgasSveikasSk.h"
#include <stdio.h>
#include <stdlib.h>
typedef struct LaikinasSkaitmuo LaikinasSkaitmuo;
struct LaikinasSkaitmuo{
	char Reiksme;
	LaikinasSkaitmuo *Kitas;
};
typedef struct LaikinasSk LaikinasSk;
struct LaikinasSk{
	LaikinasSkaitmuo *JauniausiasSkaitmuo;
	char SF;
};
void PanaikintiLaikinaSk(LaikinasSk **Sk){
    LaikinasSkaitmuo *Pointer1,*Pointer2;
    Pointer1=(*Sk)->JauniausiasSkaitmuo;
    while(Pointer1!=NULL){
        Pointer2=Pointer1->Kitas;
        free(Pointer1);
        Pointer1=Pointer2;
    };
    free(*Sk);
};
int PanaikintiSk(Sk **Skaicius){
    if((*Skaicius)==NULL){
        return 3;
    };
    if((*Skaicius)->JauniausiasSkaitmuo!=NULL)
        free((*Skaicius)->JauniausiasSkaitmuo);
    free(*Skaicius);
    *Skaicius=NULL;
    return 0;
};
int SukurtiSk(Sk **Skaicius){
    *Skaicius=(Sk*)malloc(sizeof(Sk));
    (*Skaicius)->JauniausiasSkaitmuo=NULL;
    if(*Skaicius!=NULL){
        return 0;
    }else{
        return 2;
    };
};
int NuskaitytiSk(Sk *Skaicius){
    if(Skaicius==NULL)
        return 3;
	char c;
	LaikinasSkaitmuo *PointerIVyresniSkaitmeni,*PointerISkaitmeni=NULL;
	LaikinasSk *PointerISk=(LaikinasSk*)malloc(sizeof(LaikinasSk));
	if(PointerISk==NULL)
        return 2;
	unsigned int SkaitmenuSk=0;
	char PraleidoNulius=0;
    PointerISk->JauniausiasSkaitmuo=NULL;
    PointerISk->SF=0;
	do{
        c=getchar();
        if(c!='0' && c!='-')
            PraleidoNulius=1;
        if(c>=48 && c<=57 && PraleidoNulius){
            PointerIVyresniSkaitmeni=PointerISkaitmeni;
            PointerISkaitmeni=(LaikinasSkaitmuo*)malloc(sizeof(LaikinasSkaitmuo));
            if(PointerISkaitmeni==NULL)
                return 2;
            PointerISkaitmeni->Reiksme=c-48;
            PointerISkaitmeni->Kitas=PointerIVyresniSkaitmeni;
            PointerISk->JauniausiasSkaitmuo=PointerISkaitmeni;
            SkaitmenuSk++;
        }else if(c=='-' && SkaitmenuSk==0){
            PointerISk->SF=1;
        }else if(c!='\n' && PraleidoNulius){
            PanaikintiLaikinaSk(&PointerISk);
            return 1;
        };
	}while(c!='\n');
	if(SkaitmenuSk==0 && PraleidoNulius){
        PointerISk->JauniausiasSkaitmuo=(LaikinasSkaitmuo*)malloc(sizeof(LaikinasSkaitmuo));
        if(PointerISk->JauniausiasSkaitmuo==NULL)
            return 2;
        PointerISk->JauniausiasSkaitmuo->Reiksme=0;
        PointerISk->JauniausiasSkaitmuo->Kitas=NULL;
        SkaitmenuSk++;
	}else if(SkaitmenuSk==0)
        return 1;
    Skaicius->SF=PointerISk->SF;
    Skaicius->SkaitmenuSk=SkaitmenuSk-1;
    Skaicius->JauniausiasSkaitmuo=(char*)malloc(SkaitmenuSk*sizeof(char));
    if(Skaicius->JauniausiasSkaitmuo==NULL)
        return 2;
    LaikinasSkaitmuo *Pointer1,*Pointer2;
    unsigned int i=0;
    Pointer1=PointerISk->JauniausiasSkaitmuo;
    while(Pointer1!=NULL){
        Pointer2=Pointer1->Kitas;
        Skaicius->JauniausiasSkaitmuo[i]=Pointer1->Reiksme;
        free(Pointer1);
        Pointer1=Pointer2;
        i++;
    };
    free(PointerISk);
    return 0;
};
int PriskirtiSk(Sk **Skaicius,char *KonstantosStr){
    if((*Skaicius)==NULL)
        return 3;
    if(KonstantosStr[0]=='0'){
        if((*Skaicius)->JauniausiasSkaitmuo!=NULL)
            free((*Skaicius)->JauniausiasSkaitmuo);
        (*Skaicius)->JauniausiasSkaitmuo=(char*)calloc(1,sizeof(char));
        if((*Skaicius)->JauniausiasSkaitmuo==NULL)
            return 2;
        (*Skaicius)->SkaitmenuSk=0;
        (*Skaicius)->SF=0;
        return 0;
    };
    unsigned int i,SkaitmenuSk=0;
    (*Skaicius)->SF=0;
    if(KonstantosStr[0]=='-')
        (*Skaicius)->SF=1;
    while(KonstantosStr[SkaitmenuSk+(*Skaicius)->SF]>=48 && KonstantosStr[SkaitmenuSk+(*Skaicius)->SF]<=57)
        SkaitmenuSk++;
    (*Skaicius)->SkaitmenuSk=SkaitmenuSk-1;
    (*Skaicius)->JauniausiasSkaitmuo=(char*)malloc(SkaitmenuSk*sizeof(char));
    for(i=0;SkaitmenuSk>0;i++){
        SkaitmenuSk--;
        (*Skaicius)->JauniausiasSkaitmuo[i]=KonstantosStr[SkaitmenuSk+(*Skaicius)->SF]-48;
    };
    return 0;
};
int PrilygintiSkDaliaiSk(Sk **Skaicius,Sk *Skaicius2,unsigned int Jauniausias,unsigned int Vyriausias){
    if((*Skaicius)==NULL || Skaicius2==NULL)
        return 3;
    if(Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if((*Skaicius)->JauniausiasSkaitmuo!=NULL)
        free((*Skaicius)->JauniausiasSkaitmuo);
    (*Skaicius)->SF=Skaicius2->SF;
    (*Skaicius)->SkaitmenuSk=Vyriausias-Jauniausias;
    (*Skaicius)->JauniausiasSkaitmuo=(char*)malloc(((*Skaicius)->SkaitmenuSk+1)*sizeof(char));
    unsigned int i,j=0;
    for(i=Jauniausias;i<=Vyriausias;i++){
       (*Skaicius)->JauniausiasSkaitmuo[j]=Skaicius2->JauniausiasSkaitmuo[i];
       j++;
    }
    return 0;
};
int PrilygintiSk(Sk **Skaicius,Sk *Skaicius2){
    PrilygintiSkDaliaiSk(Skaicius,Skaicius2,0,Skaicius2->SkaitmenuSk);
    return 0;
};
int ParasytiSk(Sk *Skaicius){
    if(Skaicius==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL)
        return 4;
    char *Pointer;
    char c;
    unsigned int i;
	if(Skaicius->SF!=0)
		printf("-");
	Pointer=Skaicius->JauniausiasSkaitmuo;
	for(i=Skaicius->SkaitmenuSk;i<-1;i--){
        c=*(Pointer+i)+48;
	    printf("%c",c);
	};
	return 0;
};
unsigned int max(unsigned int *a,unsigned int *b){
    if((*a)>=(*b)){
        return *a;
    }else{
        return *b;
    };
};
int PalygintiModuliusSk(Sk *Skaicius,Sk *Skaicius2){
    if(Skaicius==NULL || Skaicius2==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if(Skaicius->SkaitmenuSk>Skaicius2->SkaitmenuSk){
        return 1;
    }else if(Skaicius->SkaitmenuSk<Skaicius2->SkaitmenuSk){
        return 2;
    }else{
        unsigned int i=Skaicius->SkaitmenuSk;
        while(i<-1 && Skaicius->JauniausiasSkaitmuo[i]==Skaicius2->JauniausiasSkaitmuo[i])
            i--;
        if(i>=-1){
            return 0;
        }else if(Skaicius->JauniausiasSkaitmuo[i]>Skaicius2->JauniausiasSkaitmuo[i]){
            return 1;
        }else{
            return 2;
        };
    };
};
int PalygintiSk(Sk *Skaicius,Sk *Skaicius2){
    if(Skaicius==NULL || Skaicius2==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if((Skaicius->SkaitmenuSk>Skaicius2->SkaitmenuSk && Skaicius->SF==0) || (Skaicius->SkaitmenuSk<Skaicius2->SkaitmenuSk && Skaicius2->SF!=0)){
        return 1;
    }else if((Skaicius->SkaitmenuSk<Skaicius2->SkaitmenuSk && Skaicius2->SF==0) || (Skaicius->SkaitmenuSk>Skaicius2->SkaitmenuSk && Skaicius->SF!=0)){
        return 2;
    }else if(Skaicius->SF==Skaicius2->SF){
        unsigned int i=Skaicius->SkaitmenuSk;
        while(i<-1 && Skaicius->JauniausiasSkaitmuo[i]==Skaicius2->JauniausiasSkaitmuo[i])
            i--;
        if(i==-1){
            return 0;
        }else if(Skaicius->JauniausiasSkaitmuo[i]>Skaicius2->JauniausiasSkaitmuo[i]){
            return 1;
        }else{
            return 2;
        };
    }else if(Skaicius->SF==0){
        return 1;
    }else{
        return 2;
    };
};
unsigned int SkaitmenuSkaiciusPoSumos(Sk *Skaicius,Sk *Skaicius2){
    unsigned int i,Skaitmenu;
    char c;
    Skaitmenu=i=max(&Skaicius->SkaitmenuSk,&Skaicius2->SkaitmenuSk);
    do{
        c=0;
        if(Skaicius->SkaitmenuSk>=i)
            c+=Skaicius->JauniausiasSkaitmuo[i];
        if(Skaicius2->SkaitmenuSk>=i)
            c+=Skaicius2->JauniausiasSkaitmuo[i];
        i--;
    }while(c==9);
    if(c>9)
        Skaitmenu++;
    return Skaitmenu;
};
char SkaitmuoSk(Sk *Skaicius,unsigned int i){
    if(Skaicius->SkaitmenuSk>=i){
        return Skaicius->JauniausiasSkaitmuo[i];
    }else{
        return 0;
    };
};
int SudetiModuliusSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3){
    Sk *Rezultatas=(Sk*)malloc(sizeof(Sk));
    Rezultatas->SkaitmenuSk=SkaitmenuSkaiciusPoSumos(Skaicius,Skaicius2);
    Rezultatas->JauniausiasSkaitmuo=(char*)malloc((Rezultatas->SkaitmenuSk+1)*sizeof(char));
    if(Rezultatas->JauniausiasSkaitmuo==NULL)
        return 2;
    unsigned int i;
    char c,Mintyje=0;
    for(i=0;i<=Rezultatas->SkaitmenuSk;i++){
        c=0;
        c+=Mintyje;
        if(Skaicius->SkaitmenuSk>=i)
            c+=Skaicius->JauniausiasSkaitmuo[i];
        if(Skaicius2->SkaitmenuSk>=i)
            c+=Skaicius2->JauniausiasSkaitmuo[i];
        if(c>9){
            Mintyje=1;
            c-=10;
        }else{
            Mintyje=0;
        };
        Rezultatas->JauniausiasSkaitmuo[i]=c;
    };
    if(Skaicius->SF==0){
        Rezultatas->SF=0;
    }else{
        Rezultatas->SF=1;
    };
    if((*Skaicius3)->JauniausiasSkaitmuo!=NULL)
        free((*Skaicius3)->JauniausiasSkaitmuo);
    free((*Skaicius3));
    (*Skaicius3)=Rezultatas;
    return 0;
};
int AtimtiModuliusSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3){
    Sk *Rezultatas=(Sk*)malloc(sizeof(Sk));
    int Palyginimas=PalygintiModuliusSk(Skaicius,Skaicius2);
    if(Palyginimas==0){
        PriskirtiSk(Skaicius3,"0");
        return 0;
    }else if(Palyginimas==2){
        Sk *Laikinas;
        Laikinas=Skaicius;
        Skaicius=Skaicius2;
        Skaicius2=Laikinas;
    }else if(Palyginimas>2){
        return Palyginimas;
    };
    Rezultatas->SkaitmenuSk=max(&Skaicius->SkaitmenuSk,&Skaicius2->SkaitmenuSk);
    Rezultatas->JauniausiasSkaitmuo=(char*)malloc((Rezultatas->SkaitmenuSk+1)*sizeof(char));
    if(Rezultatas->JauniausiasSkaitmuo==NULL)
        return 2;
    unsigned int i=0;
    char c,Mintyje=0;
    while(i<=Rezultatas->SkaitmenuSk){
        c=Mintyje+SkaitmuoSk(Skaicius,i)-SkaitmuoSk(Skaicius2,i);
        if(c<=-1){
            Mintyje=-1;
            c+=10;
        }else{
            Mintyje=0;
        };
        Rezultatas->JauniausiasSkaitmuo[i]=c;
        i++;
    };
    i--;
    while(Rezultatas->JauniausiasSkaitmuo[i]==0)
        i--;
    Rezultatas->SkaitmenuSk=i;
    Rezultatas->JauniausiasSkaitmuo=(char*)realloc(Rezultatas->JauniausiasSkaitmuo,(Rezultatas->SkaitmenuSk+1)*sizeof(char));
    if((Palyginimas==1 && Skaicius->SF==0) || (Palyginimas==2 && Skaicius2->SF!=0) || Palyginimas==0){
        Rezultatas->SF=0;
    }else{
        Rezultatas->SF=1;
    };
    if((*Skaicius3)->JauniausiasSkaitmuo!=NULL)
        free((*Skaicius3)->JauniausiasSkaitmuo);
    free((*Skaicius3));
    (*Skaicius3)=Rezultatas;
    return 0;
};
int SudetiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3){
    if(Skaicius==NULL || Skaicius2==NULL || Skaicius3==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if(Skaicius->SF==Skaicius2->SF){
        SudetiModuliusSk(Skaicius,Skaicius2,Skaicius3);
    }else{
        AtimtiModuliusSk(Skaicius,Skaicius2,Skaicius3);
    };
    return 0;
};
int AtimtiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3){
    if(Skaicius==NULL || Skaicius2==NULL || Skaicius3==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if(Skaicius->SF==Skaicius2->SF){
        AtimtiModuliusSk(Skaicius,Skaicius2,Skaicius3);
    }else{
        SudetiModuliusSk(Skaicius,Skaicius2,Skaicius3);
    };
    return 0;
};
int PadaugintiIsVienzenklioSk(Sk *Skaicius,char Daugiklis,Sk **Skaicius2){/*Daugiklis turi būti nedidesnis už 9*/
    if((*Skaicius2)->JauniausiasSkaitmuo!=NULL)
            free((*Skaicius2)->JauniausiasSkaitmuo);
    if(Daugiklis==0){
        PriskirtiSk(Skaicius2,"0");
    }else{
        (*Skaicius2)->SkaitmenuSk=Skaicius->SkaitmenuSk+1;
        (*Skaicius2)->JauniausiasSkaitmuo=(char*)malloc(((*Skaicius2)->SkaitmenuSk+1)*sizeof(char));
        if((*Skaicius2)->JauniausiasSkaitmuo==NULL)
            return 2;
        unsigned int i;
        char Mintyje=0;
        for(i=0;i<=(*Skaicius2)->SkaitmenuSk;i++){
            (*Skaicius2)->JauniausiasSkaitmuo[i]=Mintyje;
            if(i<=Skaicius->SkaitmenuSk)
                (*Skaicius2)->JauniausiasSkaitmuo[i]+=Skaicius->JauniausiasSkaitmuo[i]*Daugiklis%10;
            Mintyje=Skaicius->JauniausiasSkaitmuo[i]*Daugiklis/10;
            if((*Skaicius2)->JauniausiasSkaitmuo[i]>9){
                (*Skaicius2)->JauniausiasSkaitmuo[i]-=10;
                Mintyje++;
            };
        };
        if((*Skaicius2)->JauniausiasSkaitmuo[(*Skaicius2)->SkaitmenuSk]==0){
            (*Skaicius2)->SkaitmenuSk--;
        };
    };
    return 0;
};
int PadaugintiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Skaicius3){
    if(Skaicius==NULL || Skaicius2==NULL || Skaicius3==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if((Skaicius->SkaitmenuSk==0 && Skaicius->JauniausiasSkaitmuo[0]==0) || (Skaicius2->SkaitmenuSk==0 && Skaicius2->JauniausiasSkaitmuo[0]==0)){
        PriskirtiSk(Skaicius3,"0");
        return 0;
    };
    Sk *Rezultatas=(Sk*)malloc(sizeof(Sk));
    Rezultatas->SkaitmenuSk=Skaicius->SkaitmenuSk+Skaicius2->SkaitmenuSk+1;
    Rezultatas->JauniausiasSkaitmuo=(char*)calloc((Rezultatas->SkaitmenuSk+1),sizeof(char));
    if(Rezultatas->JauniausiasSkaitmuo==NULL)
        return 2;
    Rezultatas->SF=Skaicius2->SF^Skaicius->SF;
    unsigned int i,j;
    Sk *Sumavimui;
    int ioresult;
    for(i=0;i<=Skaicius->SkaitmenuSk;i++){
        SukurtiSk(&Sumavimui);
        ioresult=PadaugintiIsVienzenklioSk(Skaicius2,Skaicius->JauniausiasSkaitmuo[i],&Sumavimui);
        if(ioresult!=0)
            return ioresult;
        char Mintyje=0;
        for(j=0;j<=Sumavimui->SkaitmenuSk;j++){
            Rezultatas->JauniausiasSkaitmuo[j+i]+=Mintyje;
            Mintyje=0;
            Rezultatas->JauniausiasSkaitmuo[j+i]+=Sumavimui->JauniausiasSkaitmuo[j];
            while(Rezultatas->JauniausiasSkaitmuo[j+i]>9){
                Rezultatas->JauniausiasSkaitmuo[j+i]-=10;
                Mintyje+=1;
            };
        };
        PanaikintiSk(&Sumavimui);
    };
    if(Rezultatas->JauniausiasSkaitmuo[Rezultatas->SkaitmenuSk]==0){
        Rezultatas->JauniausiasSkaitmuo=(char*)realloc(Rezultatas->JauniausiasSkaitmuo,Rezultatas->SkaitmenuSk*sizeof(char));
        Rezultatas->SkaitmenuSk--;
    };
    if((*Skaicius3)->JauniausiasSkaitmuo!=NULL)
        free((*Skaicius3)->JauniausiasSkaitmuo);
    free((*Skaicius3));
    (*Skaicius3)=Rezultatas;
    return 0;
};
int PadalintiSk(Sk *Skaicius,Sk *Skaicius2,Sk **Div,Sk **Mod){
    if(Skaicius==NULL || Skaicius2==NULL || Div==NULL || Mod==NULL)
        return 3;
    if(Skaicius->JauniausiasSkaitmuo==NULL || Skaicius2->JauniausiasSkaitmuo==NULL)
        return 4;
    if(Skaicius2->SkaitmenuSk==0 && Skaicius2->JauniausiasSkaitmuo[0]==0)
        return 5;
    Sk *Rezultatas=(Sk*)malloc(sizeof(Sk));
    unsigned int i,j;
    if(PalygintiModuliusSk(Skaicius,Skaicius2)==2){
        Rezultatas->SkaitmenuSk=Skaicius->SkaitmenuSk;
        Rezultatas->SF=(Skaicius->SF)^(Skaicius2->SF);
        Rezultatas->JauniausiasSkaitmuo=(char*)malloc((Rezultatas->SkaitmenuSk+1)*sizeof(char));
        if(Rezultatas->JauniausiasSkaitmuo==NULL)
            return 2;
        for(i=0;i<=Rezultatas->SkaitmenuSk;i++)
            Rezultatas->JauniausiasSkaitmuo[i]=Skaicius->JauniausiasSkaitmuo[i];
        PriskirtiSk(Div,"0");
        if((*Mod)->JauniausiasSkaitmuo!=NULL)
            free((*Mod)->JauniausiasSkaitmuo);
        (*Mod)=Rezultatas;
    }else{
        char zenklas=(Skaicius->SF)^(Skaicius2->SF);
        Skaicius->SF=Skaicius2->SF=0;
        Sk *Daliklis,*Likutis,*Constanta10,*Constanta0;
        char Skaitmuo;
        unsigned int AtsSkaitmenu=0;
        LaikinasSk *LSK=(LaikinasSk*)malloc(sizeof(LaikinasSk));
        if(LSK==NULL)
            return 2;
        LaikinasSkaitmuo *LSkaitmuo;
        LSK->JauniausiasSkaitmuo=NULL;
        SukurtiSk(&Daliklis);
        SukurtiSk(&Likutis);
        SukurtiSk(&Constanta10);
        SukurtiSk(&Constanta0);
        PriskirtiSk(&Constanta10,"10");
        PriskirtiSk(&Constanta0,"0");
        i=(Skaicius->SkaitmenuSk)-(Skaicius2->SkaitmenuSk);
        PrilygintiSkDaliaiSk(&Likutis,Skaicius,i,Skaicius->SkaitmenuSk);
        char prasuko;
        do{
            PrilygintiSk(&Daliklis,Skaicius2);
            if(PalygintiModuliusSk(Daliklis,Likutis)==1){
                PrilygintiSkDaliaiSk(&Likutis,Skaicius,(Skaicius->SkaitmenuSk)-(Skaicius2->SkaitmenuSk)-1,Skaicius->SkaitmenuSk);
                i--;
            };
            Skaitmuo=0;
            while(PalygintiModuliusSk(Daliklis,Likutis)!=1){
                SudetiSk(Daliklis,Skaicius2,&Daliklis);
                Skaitmuo++;
            };
            AtsSkaitmenu++;
            LSkaitmuo=(LaikinasSkaitmuo*)malloc(sizeof(LaikinasSkaitmuo));
            if(LSkaitmuo==NULL)
                return 2;
            LSkaitmuo->Kitas=LSK->JauniausiasSkaitmuo;
            LSkaitmuo->Reiksme=Skaitmuo;
            LSK->JauniausiasSkaitmuo=LSkaitmuo;
            AtimtiSk(Daliklis,Skaicius2,&Daliklis);
            j=i;
            AtimtiSk(Likutis,Daliklis,&Likutis);
            prasuko=0;
            while(i>0 && PalygintiModuliusSk(Skaicius2,Likutis)==1){
                i--;
                PadaugintiSk(Likutis,Constanta10,&Likutis);
                Likutis->JauniausiasSkaitmuo[0]=Skaicius->JauniausiasSkaitmuo[i];
                if(prasuko){
                    LSkaitmuo=(LaikinasSkaitmuo*)malloc(sizeof(LaikinasSkaitmuo));
                    if(LSkaitmuo==NULL)
                        return 2;
                    LSkaitmuo->Kitas=LSK->JauniausiasSkaitmuo;
                    LSkaitmuo->Reiksme=0;
                    AtsSkaitmenu++;
                    LSK->JauniausiasSkaitmuo=LSkaitmuo;
                };
                prasuko=1;
            };
        }while(PalygintiModuliusSk(Likutis,Skaicius2)!=2);
        if(j>i){
            LSkaitmuo=(LaikinasSkaitmuo*)malloc(sizeof(LaikinasSkaitmuo));
            if(LSkaitmuo==NULL)
                return 2;
            LSkaitmuo->Kitas=LSK->JauniausiasSkaitmuo;
            LSkaitmuo->Reiksme=0;
            AtsSkaitmenu++;
            LSK->JauniausiasSkaitmuo=LSkaitmuo;
        };
        (*Mod)=Likutis;
        if((*Div)->JauniausiasSkaitmuo==NULL)
            free((*Div)->JauniausiasSkaitmuo);
        (*Div)->SkaitmenuSk=AtsSkaitmenu-1;
        (*Div)->SF=zenklas;
        (*Mod)->SF=(*Div)->SF;
        (*Div)->JauniausiasSkaitmuo=(char*)malloc((AtsSkaitmenu)*sizeof(char));
        if((*Div)->JauniausiasSkaitmuo==NULL)
            return 2;
        LaikinasSkaitmuo *Pointer2,*Pointer=LSK->JauniausiasSkaitmuo;
        for(i=0;i<AtsSkaitmenu;i++){
            (*Div)->JauniausiasSkaitmuo[i]=Pointer->Reiksme;
            Pointer2=Pointer;
            Pointer=Pointer->Kitas;
            free(Pointer2);
        };
        free(LSK);
    };
    return 0;
};
void AtspausdintiKlaida(int IOResult){
    switch(IOResult){
        case 1:
            printf("Klaida: Įvestas blogas skaičiaus formatas!\n");
            break;
        case 2:
            printf("Klaida: Nėra laisvos atminties!\n");
            break;
        case 3:
            printf("Klaida: Skaičius nesukurtas!\n");
            break;
        case 4:
            printf("Klaida: Skaičius dar neturi jokios reikšmės!\n");
            break;
        case 5:
            printf("Klaida: Dalyba iš 0!\n");
            break;
    };
};
