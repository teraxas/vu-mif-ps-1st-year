#include <stdio.h>
#include <stdlib.h>
#include "SmiciausIlgasSveikasSk.h"
void Faktorialas(Sk **Skaicius){
    Sk *Skaicius2,*Constanta1,*Skaicius3;
    SukurtiSk(&Constanta1);
    SukurtiSk(&Skaicius2);
    SukurtiSk(&Skaicius3);
    PriskirtiSk(&Constanta1,"1");
    PrilygintiSk(&Skaicius2,Constanta1);
    PrilygintiSk(&Skaicius3,Constanta1);
    while(PalygintiSk(Skaicius3,(*Skaicius))!=1){
        PadaugintiSk(Skaicius2,Skaicius3,&Skaicius2);
        /*printf("Faktoriala[");
        ParasytiSk(Skaicius3);
        printf("]=");
        ParasytiSk(Skaicius2);
        printf("\n");*/
        SudetiSk(Skaicius3,Constanta1,&Skaicius3);
    };
    PrilygintiSk(Skaicius,Skaicius2);
    PanaikintiSk(&Skaicius3);
    PanaikintiSk(&Skaicius2);
    PanaikintiSk(&Constanta1);
};
int main(){
	int ioresult;
	char c,d;
	Sk *a,*b,*e;
	AtspausdintiKlaida(ioresult=SukurtiSk(&a));
	if(ioresult!=0)
        exit(ioresult);
	AtspausdintiKlaida(ioresult=SukurtiSk(&b));
	if(ioresult!=0)
        exit(ioresult);
	AtspausdintiKlaida(ioresult=SukurtiSk(&e));
	if(ioresult!=0)
        exit(ioresult);
    do{
        printf("Iveskite pradini skaiciu:");
        AtspausdintiKlaida(ioresult=NuskaitytiSk(a));
    }while(ioresult!=0);
	do{
	    printf("Pradinis skaicius=");
        ParasytiSk(a);
        printf("\n");
        printf("Iveskite 0, jei norite isjungti pavyzdine programa\n");
        printf("Iveskite 1, jei norite atimti\n");
        printf("Iveskite 2, jei norite sudeti\n");
        printf("Iveskite 3, jei norite padauginti\n");
        printf("Iveskite 4, jei norite padalinti\n");
        printf("Iveskite 5, jei norite palyginti\n");
        printf("Iveskite 6, jei norite suzinoti pradinio skaiciaus faktoriala\n");
        printf("Iveskite operacijos koda:");
        c=getchar();
        d=0;
        while(d!='\n')
            d=getchar();
        switch(c){
            case '1':
                do{
                    printf("Iveskite ka norite atimti:");
                    AtspausdintiKlaida(ioresult=NuskaitytiSk(b));
                }while(ioresult!=0);
                AtspausdintiKlaida(ioresult=AtimtiSk(a,b,&a));
                if(ioresult!=0)
                    exit(ioresult);
                printf("\nSkirtumas=");
                ParasytiSk(a);
                printf("\n");
                break;
            case '2':
                do{
                    printf("Iveskite ka norite prideti:");
                    AtspausdintiKlaida(ioresult=NuskaitytiSk(b));
                }while(ioresult!=0);
                AtspausdintiKlaida(ioresult=SudetiSk(a,b,&a));
                if(ioresult!=0)
                    exit(ioresult);
                printf("\nSuma=");
                ParasytiSk(a);
                printf("\n");
                break;
            case '3':
                do{
                    printf("Iveskite is ko norite padauginti:");
                    AtspausdintiKlaida(ioresult=NuskaitytiSk(b));
                }while(ioresult!=0);
                AtspausdintiKlaida(ioresult=PadaugintiSk(a,b,&a));
                if(ioresult!=0)
                    exit(ioresult);
                printf("\nSandauga=");
                ParasytiSk(a);
                printf("\n");
                break;
            case '4':
                do{
                    printf("Iveskite is ko norite padalinti:");
                    AtspausdintiKlaida(ioresult=NuskaitytiSk(b));
                }while(ioresult!=0);
                AtspausdintiKlaida(ioresult=PadalintiSk(a,b,&a,&e));
                if(ioresult!=0)
                    exit(ioresult);
                printf("\nDIV=");
                ParasytiSk(a);
                printf("\nMOD=");
                ParasytiSk(e);
                printf("\n");
                break;
            case '5':
                do{
                    printf("Iveskite su kuom norite palyginti:");
                    AtspausdintiKlaida(ioresult=NuskaitytiSk(b));
                }while(ioresult!=0);
                ioresult=PalygintiSk(a,b);
                if(ioresult==0){
                    ParasytiSk(a);
                    printf("=");
                    ParasytiSk(b);
                    printf("\n");
                }else if(ioresult==1){
                    ParasytiSk(a);
                    printf(">");
                    ParasytiSk(b);
                    printf("\n");
                }else if(ioresult==2){
                    ParasytiSk(a);
                    printf("<");
                    ParasytiSk(b);
                    printf("\n");
                }else{
                    AtspausdintiKlaida(ioresult);
                };
                break;
            case '6':
                Faktorialas(&a);
                if(ioresult!=0)
                    exit(ioresult);
                printf("\n");
                break;
        };
	}while(c!='0');
	PanaikintiSk(&a);
	PanaikintiSk(&b);
	PanaikintiSk(&e);
	return 0;
}
