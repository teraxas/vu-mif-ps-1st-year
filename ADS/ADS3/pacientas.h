#include <stdio.h>
#include <stdlib.h>

typedef struct patient{
	int cnt;	//visits
	int type;	//0 - group, 1 - individual
	int health;	//psychological health (health>=100 - perfect)
	int sum;	//money spent
	int nxt;	//the hour of next visit
	int vProg;	//progress of visit
} patient;

//======================================================================

//Generate random number		
		//modifier
int rNum(int mod1, int mod2);

//new patient
patient *pNew(patient *p);

//Individual visit
patient *pInVisit(patient *p, int c, int f, int hour);

//Group visit
patient *pGrVisit(patient *p, int c, int possib, int pFaster, int f, int hour);
