#include <stdio.h>
#include <stdlib.h>
#include "pacientas.h"

//Generate random number		
		//modifier
int rNum(int mod1, int mod2){
	int numb;
	numb = rand() % (mod2) + (mod1);
	return numb;
}

//new patient
patient *pNew(patient *p){
	p = malloc(sizeof(*p));
	if (p == NULL) printf("\nFAIL\n");//TEST faults
	p->cnt = 0;
	p->type = rNum(0,2);
	p->health = rNum(0,99);
	p->sum = 0;
	p->nxt = 0;
	p->vProg = 0;
	return p;
}

//Individual visit
patient *pInVisit(patient *p, int c, int f, int hour){
	p->cnt++;
	p->health = p->health + rNum(1,50);
	p->sum = p->sum + c;
	p->vProg = 0;
	p->nxt = hour + f;
	return p;
}

//Group visit
patient *pGrVisit(patient *p, int c, int possib, int pFaster, int f, int hour){
	p->cnt++;
	if (possib > rNum(1,100))
		p->health = p->health + rNum(1,(50+0.5*pFaster));
		else
			p->health = p->health + rNum(1,25);
	p->sum = p->sum + c;
	p->vProg = 0;
	p->nxt = hour + f;
	return p;
}
