#include <stdio.h>
#include <stdlib.h>
#include "or_graf.h"


int main(int argc, char **argv)
{
	//VARS
	
	int PtCnt;
	int *tmpMas, tmpCnt, tmpNr;
	ogr *g;
	int i,j;
//===================================================================
	if ( argc != 2 ) /* argc should be 3 for correct execution */
	{
		printf("\n\nKlaidingai ivesti parametrai!!!\n");
		printf( "naudojimas: %s input_file \n", argv[0] );
		return -1;
	}
	// argv[1] is a filename
	FILE *inp = fopen( argv[1], "r" );
	//returns 0 on failure
	if ( inp == 0 ){
		printf( "Could not open file\n" );
		return -1;
	}
//===================================================================
	fscanf(inp,"%d\n",&PtCnt);
	g=ogr_Init(g,PtCnt);
	printf("\n===================================================================\n");
	printf("\nOrientuotas grafas su %d taskais\n\n",ogr_GetSize(g));
	printf("===================================================================\n\n");
	
	for (i = 0; i != PtCnt; i++){
		fscanf(inp,"%d. %d/",&tmpNr,&tmpCnt);
		tmpMas = malloc(tmpCnt*sizeof(int));
		
		if (tmpNr < PtCnt && tmpMas != NULL){
			for (j = 0; j != tmpCnt; j++){
				fscanf(inp," %d",&tmpMas[j]);
			}
		}
		//nustatom taska
		ogr_SetPoint(g,tmpNr,tmpCnt,tmpMas);
		
		printf("Taskas nr %d su %d vaikais rodo i: ",tmpNr,ogr_GetChCnt(g,i));
		for (j = 0; j != ogr_GetChCnt(g,i); j++){
			printf(" %d",g->pts[i].chm[j]);
		}
		
		printf("\n");
	}
//===================================================================
	int cycle = ogr_ChkCycles(g);
	if (cycle == 1)
		printf("\nCiklu yra\n");
		else
		printf("\nCiklu ners\n");

	fclose(inp);
	return 0;
}

