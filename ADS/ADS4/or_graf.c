#include <stdio.h>
#include <stdlib.h>
#include "or_graf.h"

ogr *ogr_Init(ogr *g, int PtsCnt){
	g = malloc(sizeof(g));
	g->cnt = PtsCnt;
	g->pts = (ogr_Point *)malloc((PtsCnt) * sizeof(ogr_Point));
	return g;
}

ogr *ogr_SetPoint(ogr *g, int PtNr, int chCnt, int *chMas){
	g->pts[PtNr].ch = chCnt;
	g->pts[PtNr].chm = chMas;
	return g;
}

int *ogr_GetChNums(ogr *g, int PtNr){
	return g->pts[PtNr].chm;
}

int ogr_GetSize(ogr *g){
	return g->cnt;
}

int ogr_GetChCnt(ogr *g, int PtNr){
	return g->pts[PtNr].ch;
}

int ogr_ChkPcycle(ogr *g, int PtNr, int depth){
	depth++;//rekursijos gylis
	if (depth > ogr_GetSize(g))
		return 1;
	
	int ch = ogr_GetChCnt(g,PtNr);
	if (ch == 0) //jei vaiku nera - ne ciklas
		return 0;
	int *chm = ogr_GetChNums(g,PtNr);
	
	int i = 0;
	int rez = 0;
	
	for (i = 0; i != ch; i++){
		rez = ogr_ChkPcycle(g,chm[i],depth);
		if (rez == 1)
			return rez;
	}
	
	return rez;
}

int ogr_ChkCycles(ogr *g){
	int rez = 0;
	int i = 0;
	for (i = 0; i != ogr_GetSize(g); i++){
		rez = ogr_ChkPcycle(g, i, 0);
		if (rez == 1) return rez;
	}
	return rez;
}
