typedef struct ogr_Point {
	int ch; //child count
	int *chm; //child array (din.)
} ogr_Point;

typedef struct ogr {
	int cnt;//point count
	struct ogr_Point *pts; //points array (din.)
} ogr;

//functions
ogr *ogr_Init(ogr *g, int PtsCnt);
ogr *ogr_SetPoint(ogr *g, int PtNr, int chCnt, int *chMas);

//Get child numbers
int *ogr_GetChNums(ogr *g, int PtNr);

//counts
int ogr_GetSize(ogr *g);//Get amount of points
int ogr_GetChCnt(ogr *g, int PtNr);//Get amount of childs

//cycle chk
int ogr_ChkCycles(ogr *g);
