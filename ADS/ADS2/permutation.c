int cnt = 0;	//op count

#include <stdio.h>
#include <stdlib.h>

void print(const int *v, const int n)
{
	int i,c;
	c=0;
	printf("\n");
	if (v != 0) {
		for (i = 0; i < n; i++) {
			c++;
			printf("%4d ", v[i] );
			if (c==3){
				c=0;
				printf("| ");
			}
		}
	}
} // print

int patikr(int *mas, int *chk, int n){
	int i, j, t1,t2;
	i = 0;
	while (i != n && i != n-3){
		cnt++;//count
		
		t1=mas[chk[i]-1] + mas[chk[i+1]-1] + mas[chk[i+2]-1];
		i = i+3;
		t2=mas[chk[i]-1] + mas[chk[i+1]-1] + mas[chk[i+2]-1];
		if (t1 != t2){
			return 0;	//kai suma nelygi
		}
	}
	for (j=0; j!=n;j++)
		printf("%d ",mas[chk[j]-1]);

	printf("\n");
	return 1;
}

int rink(int *mas, int *Value, int N, int k)
{
	int i,rez;
	static int level = -1;
	
	
	
	level = level+1; Value[k] = level;

	if (level == N){
		cnt++;	//count

		rez = patikr(mas,Value, N);
	
		if (rez == 1)
			return 1;
	}
	else
		for (i = 0; i < N; i++){
			cnt++;	//count
			if (Value[i] == 0){
				rez = rink(mas, Value, N, i);
				if (rez == 1)
					return 1;
			}
		}

	level = level-1; Value[k] = 0;
	return 0;
}


int parink(int n, int *mas)
{
	int i,rez;
	int Value[n-1];

	for (i = 0; i < n; i++) {
		cnt++;	//count
		Value[i] = 0;
	}
	
	rez = rink(mas, Value, n, 0);
	if (rez == 1)
		return 1;
	
	return 0;
}


int Rcount(){
	return cnt;
}
