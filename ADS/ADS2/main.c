//Karolis Jocevicius PS1

/* 10. Ištirti, ar duotas skaičių rinkinys, gali būti išskirstytas į
 *  skaičių trejetus taip, kad skaičių visuose trejetuose suma būtų
 *  vienoda. 
 * Pvz.: {1,5,6,2,8,2,3,4,4} => (1,5,6), (2,8,2), (3,4,4)
*/
int j = 0;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "permutation.h"


// ---PRINT---
int print_mas(int *mas, int n){
	int i;
	printf("\n Nuskaityta seka: \n");
	for (i=0; i!=n; i++)
		printf("%d, ",mas[i]);
		printf("\n");
	printf("\n");
	return 0;
}

// ---MAIN---
int main(int argc, char **argv)
{
	int n = 0;	//skaiciu kiekis
	int i=0, rez=0;
	int *mas; //*bmas, *chk;
	char fn[15];
	FILE *f;
	
	printf("\n\n\n");
	printf("Programa tikrina ar ar duotas skaičių rinkinys,\n"); 
	printf("gali būti išskirstytas į trejetus taip, kad\n");
	printf("visuose trejetuose suma būtų vienoda\n\n");
	printf("Karolis Jocevičius, PS1\n\n");
	
	//failo atidarymas
	printf("Vesk failo varda: ");
	scanf("%s",fn);
	printf("\n");
	f = fopen(fn,"r");
	if (f == 0){
		printf("\nFailo nepavyko atidaryti\n");
		return -1;
	}
	
	//sekos ilgis
	printf("Vesk sekos ilgi: ");
	scanf("%d",&n);
	printf("\n");
	if (n == 0){
		printf("Sekos nariu skaicius lygus 0\n");
		return -2;
	}
	if (n == 3){
		printf("Sekos nariu trejetas yra tik vienas\n");
		return -3;
	}
	if (n % 3 != 0){
		printf("Sekos nariu skaicius nesidalija is 3\n");
		return -1;
	}
	
	//sukuriam masyva
	mas = (int *)malloc(n * sizeof(int));
	memset(mas, 0, n*sizeof(int));

	//nuskaitom ivesto ilgio seka IS FAILO
	for (i=0; i != n+1; i++){
		if (mas[i] != EOF)
			fscanf(f, "%d ", &mas[i]);
	}
	
	print_mas(mas,n);
	
	j = 0; rez =0;
	rez = parink(n,mas);

	printf("\nRezultatas:   %d",rez);
	printf("\n   Operacijos: %d\n",Rcount());

	return rez;
}

